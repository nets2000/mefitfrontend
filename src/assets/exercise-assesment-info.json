{
  "Exercises": [
    {
      "Title": "Resting Heart Rate - To Assess Aerobic Fitness",
      "DescriptionExerciseInfo": "Counting the number of beats of your resting heart rate (RHR) is a useful way of indicating your fitness progress. It should reduce as your aerobic fitness improves. Your resting heart rate (RHR) represents the number of times your heart beats each minute when you are at rest. Since a strong cardiovascular system allows your heart to pump more blood with every beat, a lower RHR tends to correspond with higher aerobic fitness. Some athletes have recorded a RHR of 40.",
      "DescriptionHowToDo": "To measure your RHR, place two fingers either on your neck, just below your jawline (carotid artery), or on your wrist (radial artery), and then count the number of beats you feel in 60 seconds. You should count the first beat as 'zero'. It is often thought that the best time to take our RHR is first thing in the morning.",
      "Results": {
        "Excellent": "60 or less",
        "Good": "61 to 80",
        "Fair": "81 to 100",
        "Poor": "101 or more"
      },
      "WhatToFillIn": "You're resting heart rate is (beats per minute):"
    },
    {
      "Title": "Push-Ups - To Assess Upper-Body Muscular Endurance",
      "DescriptionExerciseInfo": "Push-ups are a great indicator of your upper body strength and the progress of your muscle building exercise. Technically, this test measures muscular endurance rather than pure strength, as it is based on how many you can do in a fixed period of time rather than how much weight you can lift - but it is still a respectable measure of upper-body strength. Push-ups challenge the chest, shoulder and upper arm muscles - and require good core stability.",
      "DescriptionHowToDo": "Assume a push-up position (if you can't do any push-ups, then assume a modified position with your knees and lower legs on the floor). Each repetition must be executed with good technique: the body should remain in a straight line, the head should be in line with the spine, and the arms should bend to at least 90 degrees.",
      "Results": {
        "Excellent": "30 or more for men / 25 or more for women",
        "Good": "25 to 29 for men / 20 to 24 for women",
        "Fair": "20 to 24 for men / 15 to 19 for women",
        "Poor": "19 or less for men / 14 or less for women"
      },
      "WhatToFillIn": "Amount of push-ups done till to tired to continue:"
    },
    {
      "Title": "Head Turning - To Assess Neck Flexibility",
      "DescriptionExerciseInfo": "Testing the flexibility of your neck will tell you how much more stretching and mobilizing exercise you need to do to fully protect it from feeling tight. The neck is the most mobile part of the spine - or at least it should be! Often the neck gets tight on one side due to favouring that side when using the phone, carrying a bag and completing other everyday tasks.",
      "DescriptionHowToDo": "To test your neck flexibility, sit up tall and look straight ahead. Get someone to stand directly behind you as you rotate your head to the right. Ask them to note how much of your profile they can see. Eyelashes of the left eye? Nose in full profile? Now slowly return to the center and rotate your head to the left. Again, get your observer to assess how much of your profile they can see.",
      "Results": {
        "Excellent": "90 degrees",
        "Good": "85 degrees",
        "Fair": "80 degrees",
        "Poor": "79 or less"
      },
      "WhatToFillIn": "Amount of degrees able to turn neck:"
    },
    {
      "Title": "12-Minute Walk/Run - To Assess Cardio Capacity",
      "DescriptionExerciseInfo": "Kenneth Cooper - the man credited for inventing 'aerobics' - developed his 'Cooper Test' in the 1960s and the method is still widely used to measure cardiovascular fitness. The test is mainly designed for running exercise - but you can walk it if necessary. The important thing is to maintain a steady pace, rather than go hell for leather for three minutes and then crawl for the remaining nine. Cooper's results are based on a mixed gender sample of thousands of people.",
      "DescriptionHowToDo": "Use a flat, measurable route (an athletics track is ideal) or a treadmill. After a five-minute warm-up, set a stopwatch and run or walk at as fast a pace as you can sustain for the duration of the test. Record the distance and compare it to the values below.",
      "Results": {
        "Excellent": "2,35 km or more",
        "Good": "2,14 to 2,34 km",
        "Fair": "2,02 to 2,13 km",
        "Poor": "2,01 km or less"
      },
      "WhatToFillIn": "Amount of kilometers ran:"
    },
    {
      "Title": "Plank - To Assess Core Stability",
      "DescriptionExerciseInfo": "You've almost certainly heard of core stability (the strength and function of the deep stabilizing muscles of the trunk) - but how is yours? 'The plank' will give you the answer, as it is a position that you will find difficult to hold if your core stability is poor. Simply practicing this exercise movement will soon get your core stabilizers firing.",
      "DescriptionHowToDo": "Lie on your stomach with your forearms on the floor, elbows directly under your shoulders, fists facing each other. Tighten your core muscles, curl your toes under, then press down through your forearms and extend your legs to lift your body. Your head, neck, back and legs should form a straight line (like a plank of wood). Hold this position for as long as possible.",
      "Results": {
        "Excellent": "120 seconds or more",
        "Good": "60 to 119 seconds",
        "Fair": "30 to 60 seconds",
        "Poor": "29 seconds or less"
      },
      "WhatToFillIn": "Amount of time able to plank till to to tired to continue:"
    },
    {
      "Title": "Loop-The-Loop - To Assess Shoulder Mobility",
      "DescriptionExerciseInfo": "Hours working at a computer, surfing the net, watching TV, driving or simply sitting with poor posture can cause the shoulders to tighten up and the joints to lose mobility. The loop-the-loop exercise test assesses your shoulder mobility in all directions.",
      "DescriptionHowToDo": "Sit or stand with your right arm straight up, and then bend your forearm from the elbow and reach your hand down to between your shoulder blades. Then take the left arm behind you, palm outwards, and attempt to make the hands meet.",
      "Results": {
        "Excellent": "0 cm",
        "Good": "2,54 to 0,01 cm",
        "Fair": "5,08 to 2,55 cm",
        "Poor": "5,09 cm or more"
      },
      "WhatToFillIn": "Distance between hands:"
    },
    {
      "Title": "Vertical Jump - To Assess Explosive Power",
      "DescriptionExerciseInfo": "Power is the ability to exert a force quickly. It's what gets sprinters off the blocks and basketballers shooting hoops. To exert power, all your muscle fibers have to be recruited, so people with lots of endurance, but less strength, are often quite poor at it.",
      "DescriptionHowToDo": "To assess your power, stand next to a clear wall space and raise your arm which is closest to the wall as high as possible while standing with your feet flat on the floor. Mark the spot where your fingertips touch the wall. Once you've done this, leap up as high as possible, arms overhead, and touch the highest point you can on the wall. If there isn't anyone there to mark the spot you reach, you can smudge some chalk on your finger to make a mark. Now you need to subtract your standing height from your jumping height in cm and compare your result to those below.",
      "Results": {
        "Excellent": "61 to 70 cm for men / 51 to 60 cm for women",
        "Good": "51 to 60 cm for men / 41 to 50 cm for women",
        "Fair": "41 to 50 cm for men / 31 to 40 cm for women",
        "Poor": "40 cm or less for men / 30 cm or less for women"
      },
      "WhatToFillIn": "Height of jump:"
    },
    {
      "Title": "Waist-To-Hip Ratio - To Assess Body Fat Distribution",
      "DescriptionExerciseInfo": "Waist-to-hip ratio (WHR) is an assessment of the proportion of fat stored around the waist compared to the hip girth. Having an apple shape (carrying excess fat around the stomach) is worse for your health than having a pear shape (carrying excess baggage around your hips or thighs), as it is associated with heart disease and diabetes.",
      "DescriptionHowToDo": "Measure the circumference of your hips at the widest part of your buttocks with the tape held firm but not pulling. Measure the circumference of your waist at the narrowest point. To determine the ratio, divide your waist measurement by your hip measurement.",
      "Results": {
        "Excellent": "0,84 or less for men / 0,74 or less for women",
        "Good": "0,85 to 0,89 for men / 0,75 to 0,79 for women",
        "Fair": "0,90 to 0,95 for men / 0,80 to 0,85  for women",
        "Poor": "0,96 or more for men / 0,86 or more for women"
      },
      "WhatToFillIn": "Calculated difference:"
    },
    {
      "Title": "Wall Sit - To Assess Leg Strength/Endurance",
      "DescriptionExerciseInfo": "This exercise test - in which you sit on an 'invisible chair' against a wall until your thighs tighten - is a great way to test your lower body strength.",
      "DescriptionHowToDo": "Find a wall space, lean your back against it and shuffle your feet forward. Slide your back down the wall until your knee and hip joints are at a right angle, and then start your stopwatch. You should look like you are sitting on an invisible chair. Hold the position as long as you can bear while breathing freely.",
      "Results": {
        "Excellent": "76 seconds or more for men / 46 seconds or more for women",
        "Good": "58 to 75 seconds for men / 36 to 45 seconds for women",
        "Fair": "57 to 30 seconds for men / 35 to 20 seconds for women",
        "Poor": "30 seconds or less for men / 20 seconds or less for women"
      },
      "WhatToFillIn": "Amount of time able to wall sit till to to tired to continue:"
    }
  ]
}
