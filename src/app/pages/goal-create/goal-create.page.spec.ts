import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalCreatePage } from './goal-create.page';

describe('CreateGoalPage', () => {
  let component: GoalCreatePage;
  let fixture: ComponentFixture<GoalCreatePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalCreatePage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoalCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
