import { Component, OnInit } from '@angular/core';
import { Goal } from "../../models/goals.model";
import { Workout } from 'src/app/models/workout.model';
import { GoalDetailService } from "../../services/goals-detail.service";
import { ActivatedRoute } from "@angular/router";
import { WorkoutsService } from 'src/app/services/workouts.service';
import { Exercise } from 'src/app/models/exercise.model';
import { ExercisesService } from 'src/app/services/exercises.service';
import { GoalService } from 'src/app/services/goals.service';
import Swal from 'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';
import { ChartData, ChartType } from 'chart.js';
@Component({
  selector: 'app-goal-detail',
  templateUrl: './goal-detail.page.html',
  styleUrls: ['./goal-detail.page.scss']
})
export class GoalDetailPage implements OnInit {
  private _achieved: boolean = false;
  private _loading = false;
  get loading(): boolean { return this.goalDetailService.loading || this._loading }
  get error(): HttpErrorResponse | undefined { return this.goalDetailService.error; }
  get achieved(): boolean { return this._achieved }
  get percentage(): string { return this.percentage_ }

  get goal(): Goal { return this.goalDetailService.goal };
  get workouts(): Workout[] { return this.goalService.goalWorkouts; }
  get exercises(): Exercise[] { return this.goalService.goalExercises; }
  percentage_: string = '0';

  private _dataForChart: number[] = [];
  completeChart: ChartData | undefined = undefined;

  doughnutChartType: ChartType = 'doughnut';

  public chartOptions: any = {
    responsive: true,
    animations: false,
    cutout: '90%',
    plugins: {
      tooltip: { enabled: false },
    },
    hover: { mode: null },
  }

  constructor(
    private route: ActivatedRoute,
    private goalDetailService: GoalDetailService,
    private workoutService: WorkoutsService,
    private exerciseService: ExercisesService,
    private goalService: GoalService) { }

  ngOnInit(): void {
    this._loading = true;
    // Get goal ID from url parameters
    const goalId = Number(this.route.snapshot.paramMap.get("goalId"));
    this.goalDetailService.findById(goalId);
    this.goalService.findWorkoutsByGoalId(goalId)
      .add(() => {
        this.goalService.findExercisesByGoalId(goalId)
          .add(() => {
            this.percentage_ = this.goalService.completedPercentage(this.workouts, this.exercises);
            let percentage: number = parseInt(this.percentage_);

            this._dataForChart = [percentage, 100 - percentage];
            this.completeChart = {
              datasets: [{
                data: this._dataForChart,
                borderWidth: 1,
                backgroundColor: [
                  'rgba(0, 71, 232, 1)',
                  'rgba(182, 187, 198, 1)'
                ],
                borderColor: [
                  'rgba(0, 71, 232, 1)',
                  'rgba(182, 187, 198, 1)'
                ]
              }],
            };

            // If all workouts are completed, set goal as achieved
            if (percentage == 100 && !this.achieved) {
              this.goalDetailService.goalAchieved(goalId)
              .add(() => {
                this._achieved = true;
              });
            }
            this._loading = false;
          })
      });
  }
  // Function to complete a workout with workout ID for the current goal ID
  completeWorkout(workoutId: number, goalId: number) {
    this.workoutService.completeWorkout(workoutId, goalId).add(() => {
      this.percentage_ = this.goalService.completedPercentage(this.workouts, this.exercises);
      if (parseInt(this.percentage) == 100 && !this.achieved) {
        this.goalDetailService.goalAchieved(goalId);
      }
      Swal.fire({
        icon: 'success',
        title: 'Success',
        text: 'Workout completed successfully!'
      })
      // Reload the component so the percentage is updated
      this.ngOnInit();
    })
  }
  // Function to complete an exercise with an exercise ID for the current goal ID
  completeExercise(exerciseId: number | undefined, goalId: number) {
    if (exerciseId) {
      this.exerciseService.completeExercise(exerciseId, goalId)
        .add(() => {
          this.percentage_ = this.goalService.completedPercentage(this.workouts, this.exercises);
          if (parseInt(this.percentage) == 100 && !this.achieved) {
            this.goalDetailService.goalAchieved(goalId);
          }
          Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Exercise completed successfully!'
          })
          // Reload component so the percentage is updated
          this.ngOnInit();
        })
    }
  }

}
