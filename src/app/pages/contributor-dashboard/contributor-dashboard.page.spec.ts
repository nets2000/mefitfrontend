import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorDashboardPage } from './contributor-dashboard.page';

describe('ContributorDashboard', () => {
  let component: ContributorDashboardPage;
  let fixture: ComponentFixture<ContributorDashboardPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContributorDashboardPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContributorDashboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
