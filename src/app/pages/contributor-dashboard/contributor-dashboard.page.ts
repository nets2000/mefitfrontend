import { Component, OnInit } from '@angular/core';
import { ExercisesService } from 'src/app/services/exercises.service';
import { MuscleGroupService } from 'src/app/services/muscle-group.service';
import { WorkoutsService } from 'src/app/services/workouts.service';
@Component({
  selector: 'app-contributor-dashboard',
  templateUrl: './contributor-dashboard.page.html',
  styleUrls: ['./contributor-dashboard.page.scss']
})
export class ContributorDashboardPage implements OnInit {
  newProgram_: boolean = false;
  newWorkout_: boolean = false;
  newExercise_: boolean = false;
  get newProgram(): boolean { return this.newProgram_; }
  get newWorkout(): boolean { return this.newWorkout_; }
  get newExercise(): boolean { return this.newExercise_; }
  constructor() { }

  ngOnInit(): void { }
  displayProgram() {
    this.newProgram_ = !this.newProgram_;
    if (this.newWorkout)
    {
      this.newWorkout_ = false;
    }
    if (this.newExercise)
    {
      this.newExercise_ = false;
    }
  }
  displayWorkout() {
    this.newWorkout_ = !this.newWorkout_;
    if (this.newExercise)
    {
      this.newExercise_ = false;
    }
    if (this.newProgram)
    {
      this.newProgram_ = false;
    }
  }
  displayExercise() {
    this.newExercise_ = !this.newExercise_;
    if (this.newWorkout)
    {
      this.newWorkout_ = false;
    }
    if (this.newProgram)
    {
      this.newProgram_ = false;
    }
  }


}
