import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage {

  constructor(
    private readonly router: Router
  ) { }

  // Navigate user to the login page
  public HandleLogin(): void {
    this.router.navigateByUrl("/");
  }
}
