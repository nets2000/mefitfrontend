import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { WorkoutsService } from "../../services/workouts.service";
import { Workout } from "../../models/workout.model";
import { Observable } from "rxjs";
import { MuscleGroup } from "../../models/muscleGroup.model";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { WorkoutTypes } from 'src/app/enums/workout-types.enum';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.page.html',
  styleUrls: ['./workouts.page.scss']
})
export class WorkoutsPage implements OnInit {

  @Input() canAddToProgram: boolean = false;

  @ViewChild('paginator') paginator?: MatPaginator;
  private _pageSize: number = 3;
  private _pageSizeOptions: number[] = [3, 10, 25, 100];

  selectedWorkoutType: string = "";

  private _workoutTypes: string[] = [];

  get selectedWorkouts(): Workout[] {
    return this.workoutsService.selectedWorkouts;
  }

  get workouts(): Workout[] {
    return this.workoutsService.workouts;
  }

  get limitWorkouts(): Workout[] {
    return this.workoutsService.limitWorkouts;
  }

  get loading(): boolean {
    return this.workoutsService.loading;
  }

  get error(): HttpErrorResponse | undefined {
    return this.workoutsService.error;
  }

  get pageSizeOptions(): number[] {
    return this._pageSizeOptions;
  }

  get workoutTypes(): string[] {
    return this._workoutTypes;
  }

  constructor(
    private readonly workoutsService: WorkoutsService
  ) { }

  // Fill list of all workout types to use for dropdown an show all programs types on load in if workouts can be added to a program
  ngOnInit(): void {
    this._workoutTypes.push("All");
    const workoutTypes = Object.values(WorkoutTypes);
    workoutTypes.forEach((value) => {
      this._workoutTypes.push(value);
    });

    if (this.canAddToProgram) {
      this.ChangeWorkoutType('All');
    }
  }

  // The function that gets called when a different option is selected 
  ChangeWorkoutTypeEvent(e: any) {
    this.ChangeWorkoutType(e.target.value);
  }

  // Change what workouts are shown based on the given type and go back to the first page
  private ChangeWorkoutType(type: string) {
    this.selectedWorkoutType = type;
    this.workoutsService.getSelectedWorkouts(this.selectedWorkoutType, this._pageSize);
    this.paginator?.firstPage();
  }

  // The function that gets called when going to another page
  OnPageChange(event: PageEvent) {
    this._pageSize = event.pageSize;
    this.workoutsService.limitWorkouts = [];

    const startIndex = event.pageIndex * this._pageSize;
    let endIndex = startIndex + this._pageSize;
    if (endIndex > this.selectedWorkouts.length) {
      endIndex = this.selectedWorkouts.length;
    }

    // Set the shown workouts to what should be visible based on the start/ end index
    this.workoutsService.limitWorkouts = this.selectedWorkouts.slice(startIndex, endIndex);
  }

  // Change the amount of workouts shown on the page and go back to the first page
  public ChangePageSize(e: any) {
    this.paginator?._changePageSize(Number.parseInt(e.target.value));
    this.paginator?.firstPage();
  }
}
