import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ProfilePic } from 'src/app/models/profile-pic.model';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import { environment } from "../../../environments/environment";

const { apiUrl } = environment;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  private _editProfilePage: boolean = false;

  private _baseImage: string = "../../../../assets/baseProfilePic.jpg";
  private _imgURL: string = this._baseImage;

  get editProfilePage(): boolean {
    return this._editProfilePage;
  }

  get imgURL(): string {
    return this._imgURL;
  }
  get error():HttpErrorResponse | undefined {
    return this.profileManager.error;
  }

  constructor(
    private readonly profileManager: ProfileManagerService,
    private readonly http: HttpClient
  ) { }

  // Gets the current profile image
  ngOnInit(): void {
    this.GetProfileImage();
  }

  // Gets the current profile image and sets the imgURL to the correct data
  public GetProfileImage(): void {
    this.http.get<ProfilePic>(`${apiUrl}/users/${this.profileManager.currentUser?.id}/profilepic`)
    .subscribe({
      next: (profilePic: ProfilePic) => {
        if (profilePic) {
          this._imgURL = `data:${profilePic.type};base64,${profilePic.pic}`;
        }
      },
      error: (error: HttpErrorResponse) => {
        console.log(error.message)
      }
    })
  }

  // Switches the state of the profile page between edit/ show state
  public SwitchEditProfilePage() {
    this._editProfilePage = !this._editProfilePage;
  }
}
