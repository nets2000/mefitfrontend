import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  constructor(private router: Router,
    private readonly profileManager: ProfileManagerService
  ) { }

  // Checks whether the profile exists and dependent on that and if the user is logged in keycloak grabs user data or sends user to welcome page or doesn't do anything
  ngOnInit(): void {
    let profileExist : boolean = this.profileManager.GetIfProfileExists();
    if (keycloak.authenticated && !profileExist) {
      this.profileManager.GetProfile(this);
    } else if (keycloak.authenticated && profileExist) {
      this.NavigateUser("/goals");
    }
  }

  // Send login to keycloak
  doLogin(): void {
    keycloak.login();
  }

  // Navigate user to given url
  public NavigateUser(url: string): void {
    this.router.navigateByUrl(url);
  }
}
