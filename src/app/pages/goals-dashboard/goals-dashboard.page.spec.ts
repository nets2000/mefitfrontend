import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalsDashboardPage } from './goals-dashboard.page';

describe('GoalsDashboardPage', () => {
  let component: GoalsDashboardPage;
  let fixture: ComponentFixture<GoalsDashboardPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalsDashboardPage ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoalsDashboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
