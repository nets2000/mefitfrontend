import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { finalize, map, Observable } from 'rxjs';
import { AppComponent } from 'src/app/app.component';
import { Goal } from 'src/app/models/goals.model';
import { GoalService } from 'src/app/services/goals.service';
import keycloak from 'src/keycloak';
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {Profile} from "../../models/profile.model";
import {ProfileManagerService} from "../../services/profile-manager.service";
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-goals',
  templateUrl: './goals-dashboard.page.html',
  styleUrls: ['./goals-dashboard.page.scss'],
})
export class GoalsDashboardPage implements OnInit {
  @ViewChild('paginator') paginator?: MatPaginator;
  private fullName_!:string;
  private weekday_!: string;
  private today_!: number;
  private month_!: string;
  private daysLeft_: number = 0;
  private _pageSize : number = 1;
  private _pageSizeHistory : number = 3;
  private _selectedHistory : Goal[] = [];
  private _selectedGoals : Goal[] = [];
  private MONTHS: string[] = ["January", "February", "March", "April", "May", "June", "July", "Augustus", "Sep", "October", "November", "December"];
  private DAYS: string[] = ["Sunday", "Monday", "Tuesday", "Wed", "Thursday", "Friday", "Saturday"];
  private _loading:boolean = false;
  get loading():boolean { return this._loading; }
  get fullName():string { return this.fullName_; }
  get currentGoals(): Goal[] { return this.goalService.currentGoals; }
  get previousGoals(): Goal[] { return this.goalService.previousGoals; }
  get selectedGoals(): Goal[] { return this._selectedGoals; }
  get selectedHistory(): Goal[] { return this._selectedHistory; }

  get weekday():string { return this.weekday_ }
  get today():number { return this.today_ }
  get month():string { return this.month_}
  get daysLeft():number { return this.daysLeft_ }

  get profile(): Profile {
    return this.profileManager.currentProfile!;
  }

  get error():HttpErrorResponse | undefined { return this.goalService.error; }

  constructor(
    private goalService: GoalService,
    private readonly profileManager: ProfileManagerService
  ) {}

  ngOnInit(): void {
    this._loading = true;
    let date = new Date();
    this.weekday_ = this.DAYS[date.getDay()];
    this.month_ = this.MONTHS[date.getMonth()];
    this.today_ = date.getDate();
    this.daysLeft_ = 7 - date.getDay();
    if (this.currentGoals.length == 0) this.goalService.findCurrent().add(() => {
      if (this.previousGoals.length == 0) this.goalService.findHistory();
    })
    if (keycloak.tokenParsed && keycloak.tokenParsed.name)
    {
      this.fullName_ = keycloak.tokenParsed.name;
    }
    this.SetSelectedGoals();
    this.SetSelectedHistory();
  }

  private SetSelectedGoals() {
    if (this.currentGoals.length > 0) {
      this.SetPageContent(0);
      this.paginator?.firstPage();
    } else {
      setTimeout(() => {
        this.SetSelectedGoals();
      }, 100);
    }
    this._loading = false;
  }

  OnPageChange(event: PageEvent) {
    this._selectedGoals = [];
    const startIndex = event.pageIndex * this._pageSize;

    this.SetPageContent(startIndex);
  }

  private SetPageContent(startIndex: number) {
    let endIndex = startIndex + this._pageSize;
    if (endIndex > this.currentGoals.length) {
      endIndex = this.currentGoals.length;
    }
    this._selectedGoals = this.currentGoals.slice(startIndex, endIndex);
  }

  private SetSelectedHistory() {
    if (this.previousGoals.length > 0) {
      this.SetHistoryContent(0);
      this.paginator?.firstPage();
    } else {
      setTimeout(() => {
        this.SetSelectedHistory();
      }, 100);
    }
  }

  OnHistoryChange(event: PageEvent) {
    this._selectedHistory = [];
    const startIndex = event.pageIndex * this._pageSizeHistory;

    this.SetHistoryContent(startIndex);
  }

  private SetHistoryContent(startIndex: number) {
    let endIndex = startIndex + this._pageSizeHistory;
    if (endIndex > this.previousGoals.length) {
      endIndex = this.previousGoals.length;
    }
    this._selectedHistory = this.previousGoals.slice(startIndex, endIndex);
  }


}
