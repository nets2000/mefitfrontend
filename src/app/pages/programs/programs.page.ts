import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FitnessProgram } from 'src/app/models/fitness-program.model';
import { FitnessProgramService } from 'src/app/services/fitness-program.service';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.page.html',
  styleUrls: ['./programs.page.scss']
})
export class ProgramsPage implements OnInit {

  get allFitnessPrograms(): FitnessProgram[] {
    return this.fitnessProgramService.allFitnessPrograms;
  }
  get error(): HttpErrorResponse | undefined {
    return this.fitnessProgramService.error;
  }

  constructor(
    private readonly fitnessProgramService: FitnessProgramService
  ) { }
  ngOnInit(): void {
      this.fitnessProgramService.GetAllPrograms();
  }
}
