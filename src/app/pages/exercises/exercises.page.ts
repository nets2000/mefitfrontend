import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import {Observable} from "rxjs";

import {Exercise} from "../../models/exercise.model";
import {ExercisesService} from "../../services/exercises.service";
import {MuscleGroup} from "../../models/muscleGroup.model";
import {MuscleGroupService} from "../../services/muscle-group.service";
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.page.html',
  styleUrls: ['./exercises.page.scss']
})
export class ExercisesPage  {
  @ViewChild('paginator') paginator?: MatPaginator;
  private _pageSize : number = 10;
  private _pageSizeOptions: number[] = [10, 25, 100];
  
  selectedMuscleGroup: number = 0;

  get selectedExercises(): Exercise[] {
    return this.exercisesService.selectedExercises;
  }

  get limitExercises(): Exercise[] {
    return this.exercisesService.limitExercises;
  }

  get muscleGroups(): MuscleGroup[] {
    return this.muscleGroupService.muscleGroups;
  }

  get loading(): boolean {
    return this.exercisesService.loading;
  }

  get error(): HttpErrorResponse | undefined {
    return this.exercisesService.error;
  }

  get pageSizeOptions() {
    return this._pageSizeOptions;
  }

  constructor(
    private readonly exercisesService: ExercisesService,
    private readonly muscleGroupService: MuscleGroupService
  ) { }

  // Changes to show all exercises that have the given muscle group
  changeMuscleGroup(e: any) {
    this.selectedMuscleGroup = e.target.value;
    if (this.selectedMuscleGroup == 0) {
      this.exercisesService.selectedExercises = this.exercisesService.exercises;
    } else {
      this.exercisesService.findExercisesWithMuscleGroup(this.selectedMuscleGroup);
    }

    // Reset page to first page
    this.paginator?.firstPage();
    this.SetPageContent(0);
  }

  // The event that gets called when a new page is shown
  OnPageChange(event: PageEvent) {
    this.exercisesService.limitExercises = [];
    const startIndex = event.pageIndex * this._pageSize;

    this.SetPageContent(startIndex);
  }

  // Sets the limit exercises to the given page indexes
  private SetPageContent(startIndex: number) {
    let endIndex = startIndex + this._pageSize;
    if (endIndex > this.selectedExercises.length) {
      endIndex = this.selectedExercises.length;
    }
    this.exercisesService.limitExercises = this.selectedExercises.slice(startIndex, endIndex);
  }

  // The event that gets called when the page size gets changed
  public ChangePageSizeEvent(e: any) {
    this._pageSize = Number.parseInt(e.target.value);
    this.ChangePageSize(this._pageSize);
  }

  // Changes the page size to the given number
  private ChangePageSize(pageSize: number) {
    this.paginator?._changePageSize(pageSize);
    this.paginator?.firstPage();
  }
}
