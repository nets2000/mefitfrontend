import { Exercise } from "./exercise.model";

// All the data that a workout contains
export interface Workout {
  id: number;
  name: string;
  type: string;
  complete: boolean;
  exercises?: Exercise[];
}
