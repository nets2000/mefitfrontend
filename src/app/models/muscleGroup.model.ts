// The data of a muscle group
export interface MuscleGroup {
  id: number;
  name: string;
}
