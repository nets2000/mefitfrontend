// The data within an exercise
export interface Exercise {
  id?: number;
  name: string;
  description: string;
  totalSets: number;
  reps: number;
  minReps: number;
  maxReps: number;
  image: string;
  vidLink: string;
  muscleGroupIds: number[];
  complete?: boolean;
  userId:number;
}
