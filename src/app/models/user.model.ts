// The standard data that a user contains
export interface User {
    id: number;
    firstName: string;
    lastName: string;
    userId: string;
    wantsToBeContributor: boolean;
    isContributor: boolean;
    isAdmin: boolean;
    profilePicId: number | null;
}

// The data required for creating a new user
export interface UserCreate {
    password: string;
    firstName: string;
    lastName: string;
    userId: string;
    wantsToBeContributor: string;
    isContributor: string;
    isAdmin: string;
    profilePicId: number | null;
}

// The data required for updating the user
export interface UserUpdate {
    firstName: string;
    lastName: string;
    wantsToBeContributor: string;
    isContributor: string;
    isAdmin: string;
}