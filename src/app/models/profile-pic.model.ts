// The data in the profile pic
export interface ProfilePic {
    id: number;
    name: string;
    type: string;
    pic: string;
}

// The data required for creating a profile pic
export interface ProfilePicCreate {
    name: string;
    type: string;
    pic: string;
}

// The data required to update the profile pic
export interface ProfilePicUpdate {
    name: string;
    type: string;
    pic: string;
}