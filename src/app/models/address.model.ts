// The data within an address that is gotten from teh database
export interface Address {
    id: number;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    postalCode: string;
    city: string;
    country: string;
}

// The data required to create a new address
export interface AddressCreate {
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    postalCode: string;
    city: string;
    country: string;
    profileIds: number[];
}