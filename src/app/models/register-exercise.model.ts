// The data of an register exercise
export interface RegisterExercise {
    Title: string;
    DescriptionExerciseInfo: string;
    DescriptionHowToDo: string;
    WhatToFillIn: string;
    Results: Results;
}

// All the possible results from the register exercise
export interface Results {
    Excellent: string;
    Good: string;
    Fair: string;
    Poor: string;
}