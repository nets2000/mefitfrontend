import { Workout } from "./workout.model";

export interface Goal {
    id: number;
    endDate: string;
    achieved: boolean;
    fitnessProgramId: number;
    current: boolean
    workouts: Workout[],
}
