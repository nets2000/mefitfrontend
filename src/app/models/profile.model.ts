import { FitnessLevel } from "../enums/fitness-level.enum";

// The data that a profile contains
export interface Profile {
    id: number;
    height: number;
    weight: number;
    disabilities: string;
    medicalConditions: string;
    fitnessEvaluation: string;
    fitnessLevel: FitnessLevel;
    addressId: number;
    userId: number;
}

// The data required for creating a profile
export interface ProfileCreate {
    weight: number;
    height: number;
    medicalConditions: string;
    disabilities: string;
    fitnessEvaluation: string;
    userId: number;
    addressId: number;
}

// The data required for updating a model
export interface ProfileUpdate {
    weight: number;
    height: number;
    medicalConditions: string;
    disabilities: string;
    fitnessEvaluation: string;
    workoutIds: number[];
    goalIds: number[];
}