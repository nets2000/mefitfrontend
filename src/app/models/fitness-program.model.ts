import { ProgramCategory } from "../enums/programCategory.enum";

// The data within a program
export interface FitnessProgram {
    id: number;
    name: string;
    category: ProgramCategory;
    profileIds: number[];
    workoutIds: number[];
}

// The data required to create a new program
export interface CreateFitnessProgram {
    name: string;
    category: ProgramCategory;
}

// The data required to update a program
export interface UpdateFitnessProgram {
    id: number;
    name: string;
    category: ProgramCategory;
}