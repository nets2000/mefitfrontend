import { Injectable } from '@angular/core';
import {MuscleGroup} from "../models/muscleGroup.model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Exercise} from "../models/exercise.model";
import {finalize} from "rxjs";
import {environment} from "../../environments/environment";

const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class MuscleGroupService {

  private _muscleGroups: MuscleGroup[] = [];

  private _loading: boolean = false;
  private _error?: HttpErrorResponse;

  get loading(): boolean { return this._loading }
  get error(): HttpErrorResponse | undefined { return this._error }
  get muscleGroups(): MuscleGroup[] { return this._muscleGroups }

  constructor(private readonly http: HttpClient) { }

  // Get all muscle groups
  public findAllMuscleGroups(): void {
    this._loading = true;
    this.http.get<MuscleGroup[]>(`${apiUrl}/musclegroups`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (muscleGroups: MuscleGroup[]) => {
          this._muscleGroups = muscleGroups;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }
}
