import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {finalize, Subscription} from "rxjs";
import {Exercise} from "../models/exercise.model";
import {environment} from "../../environments/environment";
import {Workout} from "../models/workout.model";
import { WorkoutsService } from './workouts.service';
import { WorkoutDetailComponent } from 'src/app/components/workout/workout-detail/workout-detail.component';
import { ExercisesService } from './exercises.service';
const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class WorkoutDetailService {

  private _error?: HttpErrorResponse;
  private _loading: boolean = false;
  private _goalWorkouts:Workout[] = [];
  private _workoutExercises: Exercise[] = [];
  get error(): HttpErrorResponse | undefined { return this._error; }
  get loading(): boolean { return this._loading; }
  get workoutExercises(): Exercise[] { return this._workoutExercises; }
  get goalWorkouts():Workout[] { return this._goalWorkouts; }

  constructor(
    private readonly http: HttpClient,
    private readonly workoutService: WorkoutsService,
    private readonly exerciseService: ExercisesService
    ) { }

  // Return a given workout based on the given id
  public findWorkoutById(workoutId: number): Workout {
    return this.workoutService.workouts.find(workout => workout.id = workoutId)!;
  }

  // Get all exercises from a given workout and set these exercises to the given workout detail component
  public findExercisesFromWorkout(workoutId: number, workOutDetail?: WorkoutDetailComponent): Subscription {
    return this.http.get<Exercise[]>(`${apiUrl}/workouts/${workoutId}/exercises`)
    .subscribe({
      next: (exercises: Exercise[]) => {
        let requiredExercises: Exercise[] = [];
        // The returned exercises did not have muscle group ids
        // Find the exercises from the exercise service where they have muscle group ids
        for (let i = 0; i < exercises.length; i++) {
          requiredExercises.push(this.exerciseService.exercises.find(exercise => exercise.id == exercises[i].id)!);
        }
        if (workOutDetail)
        {
          workOutDetail.exercises = requiredExercises;
        }
        else {
          this._workoutExercises = requiredExercises;
        }
      },
      error: (error: HttpErrorResponse) => {
        this._error = error;
      }
    })
  }
}
