import { Injectable } from '@angular/core';
import {finalize, Observable, Subscription} from "rxjs";
import {Goal} from "../models/goals.model";
import { Workout } from '../models/workout.model';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import { Exercise } from '../models/exercise.model';

const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class GoalDetailService {
  private _loading:boolean = false;
  private _error?:HttpErrorResponse;
  private _workouts:Workout[] = [];
  private _completedWorkouts:Workout[] = [];
  private _exercises:Exercise[] = [];
  private _goal!:Goal;
  get loading(): boolean { 
    return this._loading; 
  }
  get goal(): Goal { 
    return this._goal; 
  }
  get workouts():Workout[] { 
    return this._workouts; 
  }
  get completedWorkouts():Workout[] { 
    return this._completedWorkouts; 
  }
  get exercises():Exercise[] { 
    return this._exercises; 
  }
  get error():HttpErrorResponse | undefined { 
    return this._error;
   }
  constructor(private readonly http: HttpClient){ }
  // Function to get goal with goal ID
  findById(GoalId: number) : void {
    this._loading = true;
    this.http.get<Goal>(apiUrl + "/goals/" + GoalId)
    .pipe(finalize(() => {
      this._loading = false;
    }))
    .subscribe({
      next: (goal) => {
        this._goal = goal;
      },
      error: (error : HttpErrorResponse) => {
        this._error = error;
      }
    })
  }
  /*
    Function to get all exercises that belong to a given goalId
    Returns a Subscription so the components can add a finalizer if needed
  */
  findExercisesById(GoalId: number)
  {
    this._exercises = [];
    this._loading = true;
    return this.http.get<Exercise[]>(apiUrl + "/goals/" + GoalId + "/exercises")
    .pipe(finalize(() => {
      this._loading = false;
    }))
    .subscribe({
      next: (exercises) => {
        exercises.forEach(exercise => {
          this.exercises.push(exercise);
        });
      },
      error: (error: HttpErrorResponse) => {
        this._error = error;
      }
    }) 
  }
  /*
    Function to complete a goal. Updates the endDate to match the current date
    Returns a Subscription so the components can add a finalizer if needed
  */
  goalAchieved(GoalId: number) : Subscription
  {
    let date = new Date();
    return this.http.patch(`${apiUrl}/goals/${GoalId}/achieve`, {
      "id":GoalId, 
      "achieved":true, 
      "current":false, 
      "endDate": date.toISOString().split('Z')[0]})
      .subscribe();
  }
}
