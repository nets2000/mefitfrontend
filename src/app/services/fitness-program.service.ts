import { Injectable } from '@angular/core';
import { FitnessProgram } from '../models/fitness-program.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { Observable, map } from 'rxjs';
import { Workout } from '../models/workout.model';
import { ProgramFormComponent } from '../components/program/program-form/program-form.component';

const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class FitnessProgramService {

  private _allFitnessPrograms: FitnessProgram[] = [];
  private _totalFitnessPrograms: FitnessProgram[] = [];
  private _loading: boolean = false;
  private _error?:HttpErrorResponse;
  private _currentlyEditedProgramWorkoutIndexes: number[] = [];

  constructor(
    private readonly http: HttpClient
  ) { }

  get allFitnessPrograms(): FitnessProgram[] {
    return this._allFitnessPrograms;
  }
  get totalFitnessPrograms(): FitnessProgram[] { 
    return this._totalFitnessPrograms; 
  }
  get loading(): boolean { 
    return this._loading; 
  }

  get currentlyEditedProgramWorkoutIndexes(): number[] {
    return this._currentlyEditedProgramWorkoutIndexes;
  }

  get error() : HttpErrorResponse | undefined {
    return this._error;
  } 
  set currentlyEditedProgramWorkoutIndexes(currentlyEditedProgramWorkoutIndexes: number[]) {
    this._currentlyEditedProgramWorkoutIndexes = currentlyEditedProgramWorkoutIndexes;
  }

  // Get all fitness programs
  public GetAllPrograms(programFormComponent: ProgramFormComponent | undefined = undefined): void {
    this.http.get<FitnessProgram[]>(`${apiUrl}/fitnessprogram`)
      .subscribe({
        next: (fitnessPrograms: FitnessProgram[]) => {
          this._allFitnessPrograms = fitnessPrograms;

          // After adding a new program, this function is also called
          // Signal that the fitness programs have been updated and load the show fitness program page again
          if (programFormComponent) {
            programFormComponent.FinishAddingWorkoutsToProgram();
          }
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Gets the workouts of a given program
  public GetWorkoutsOfProgram(programId: number): Observable<Workout[]> {
    return this.http.get<Workout[]>(`${apiUrl}/fitnessprogram/${programId}/workouts`)
    .pipe(
      map((response: Workout[]) => {
        return response;
      })
      )
  }

  // Add/ removes an workout id from the currentlyEditedProgramWorkoutIndexes
  public EditCurrentlyEditedProgramWorkoutIndexes(workoutId: number) {
    const elementIndex = this.currentlyEditedProgramWorkoutIndexes.indexOf(workoutId, 0);
    if (elementIndex > -1) {
      this.currentlyEditedProgramWorkoutIndexes.splice(elementIndex, 1);
    } else {
      this.currentlyEditedProgramWorkoutIndexes.push(workoutId);
    }
  }
}
