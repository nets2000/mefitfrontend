import { Injectable } from '@angular/core';
import { LoginPage } from '../pages/login/login.page';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { finalize } from 'rxjs';
import keycloak from 'src/keycloak';
import { Profile, ProfileUpdate, ProfileCreate } from '../models/profile.model';
import { ExercisesService } from './exercises.service';
import { MuscleGroupService } from './muscle-group.service';
import { WorkoutsService } from './workouts.service';
import { FitnessProgramService } from './fitness-program.service';
import { User, UserUpdate } from '../models/user.model';
import { Address, AddressCreate } from '../models/address.model';
import { FitnessLevel } from '../enums/fitness-level.enum';

const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class ProfileManagerService {

  private _currentProfile?: Profile = undefined;
  private _currentUser?: User = undefined;
  private _currentAddress?: Address = undefined;
  private _error?:HttpErrorResponse;
  constructor(
    private readonly http: HttpClient,
    private readonly exerciseService: ExercisesService,
    private readonly muscleGroupService: MuscleGroupService,
    private readonly workoutsService: WorkoutsService,
    private readonly programsService: FitnessProgramService
  ) { }

  get currentProfile(): Profile | undefined {
    return this._currentProfile;
  }

  get currentUser(): User | undefined {
    return this._currentUser;
  }

  get currentAddress(): Address | undefined {
    return this._currentAddress;
  }
  get error(): HttpErrorResponse | undefined {
    return this._error;
  }
  
  // This function is always called on start-up of the website
  // Get the current profile and navigate the user to the required page
  public GetProfile(loginPage: LoginPage): void {
    this.exerciseService.findAllExercises();
    this.muscleGroupService.findAllMuscleGroups();
    this.workoutsService.getAllWorkouts();
    this.programsService.GetAllPrograms();

    // Check whether user exist based on the keycloak id that is also saved in the database
    // If user does not exist, send user to register page else fetch all other profile information
    this.http.get<Profile>(`${apiUrl}/users/${keycloak.tokenParsed?.sub}/profile`)
    this.http.get<any>(`${apiUrl}/users/${keycloak.tokenParsed?.sub}/profile`)
      .pipe(
        finalize(() => {
          if (this._currentProfile != undefined) {
            this.GetUser(loginPage);
          } else {
            loginPage.NavigateUser("/register");
          }
        })
      )
      .subscribe({
        next: (profile: Profile) => {
          this._currentProfile = profile;
          this._currentProfile.fitnessLevel = FitnessLevel[profile.fitnessEvaluation as keyof typeof FitnessLevel];
        },
        error: (error: HttpErrorResponse) => {
          if (error.status == 404) {
            loginPage.NavigateUser("/register");
          } else {
            this._error = error;
          }
        }
      })
  }

  // Get the current user of the program
  public GetUser(loginPage: LoginPage) {
    this.http.get<User>(`${apiUrl}/users/${this._currentProfile?.userId}`)
    .pipe(
      finalize(() => {
        this.GetAddress(loginPage);
      })
    )
    .subscribe({
      next: (user: User) => {
        this._currentUser = user;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error;
      }
    })
  }

  // Get the current address of the program
  public GetAddress(loginPage: LoginPage) {
    this.http.get<Address>(`${apiUrl}/addresses/${this._currentProfile?.addressId}`)
    .pipe(
      finalize(() => {
        // Send user to the welcome page of the program
        loginPage.NavigateUser("/goals");
      })
    )
    .subscribe({
      next: (address: Address) => {
        this._currentAddress = address;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error;
      }
    })
  }

  // Updates the current user to the given data
  public UpdateCurrentUser(updatedUser: UserUpdate, profilePicId: number | null): void {
    this._currentUser!.wantsToBeContributor = (/true/i).test(updatedUser.wantsToBeContributor) ? true : false;
    this._currentUser!.profilePicId = profilePicId;
  }

  // Updates the current address to the given data
  public UpdateCurrentAddress(updatedAddress: AddressCreate): void {
    this._currentAddress!.addressLine1 = updatedAddress.addressLine1;
    this._currentAddress!.addressLine2 = updatedAddress.addressLine2;
    this._currentAddress!.addressLine3 = updatedAddress.addressLine3;
    this._currentAddress!.country = updatedAddress.country;
    this._currentAddress!.city = updatedAddress.city;
    this._currentAddress!.postalCode = updatedAddress.postalCode;
  }

  // Updates current profile to current data
  public UpdateCurrentProfile(updatedProfile: ProfileUpdate): void {
    this._currentProfile!.height = updatedProfile.height;
    this._currentProfile!.weight = updatedProfile.weight;
    this._currentProfile!.disabilities = updatedProfile.disabilities;
    this._currentProfile!.medicalConditions = updatedProfile.medicalConditions;
    this._currentProfile!.fitnessEvaluation = updatedProfile.fitnessEvaluation;
  }

  // Gets the id of the current profile
  public GetProfileID(): number {
    return this._currentProfile!.id;
  }

  // Sets the profile to undefined
  public SetProfileToUndefined(): void {
    this._currentProfile = undefined;
  }

  // Checks whether the program has a profile and returns true if its has else it returns false
  public GetIfProfileExists(): boolean {
    if (this._currentProfile) {
      return true;
    }
    return false;
  }
}
