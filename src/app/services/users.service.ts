import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Workout } from '../models/workout.model';
import { environment } from 'src/environments/environment';
import { ProfileManagerService } from './profile-manager.service';
import { finalize, Subscription } from 'rxjs';

const { apiUrl } = environment;
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private _workoutsCreated:Workout[] = []
  private _loading:boolean = false;
  private _error?:HttpErrorResponse;
  get loading():boolean { 
    return this._loading; 
  }
  get workoutsCreated():Workout[] { 
    return this._workoutsCreated; 
  }
  get error():HttpErrorResponse | undefined {
    return this._error;
  }
  constructor(
    private http:HttpClient,
    private profileService:ProfileManagerService) { }
  /*
    Function to get the current users' previously created workouts. 
    Returns a Subscription so the components can add a finalizer if needed.
  */
  public getPastUserWorkoutCreations() : Subscription
  {
    this._loading = true;
    return this.http.get<Workout[]>(`${apiUrl}/users/${this.profileService.currentProfile?.userId}/workouts-created`)
    .pipe(finalize(() => {
      this._loading = false;
    }))
    .subscribe({
      next: (workouts) => {
        this._workoutsCreated = workouts;
      }, error: (error:HttpErrorResponse) => {
        this._error = error;
      }
    });
  }
}
