import { Injectable, ɵgetUnknownElementStrictMode } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Workout } from "../models/workout.model";
import { finalize, Observable, Subscription } from "rxjs";
const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class WorkoutsService {

  private _workouts: Workout[] = [];
  private _selectedWorkouts: Workout[] = [];
  private _limitWorkouts: Workout[] = this.selectedWorkouts.slice(0,3);
  private _error?: HttpErrorResponse;
  private _loading: boolean = false;

  get workouts(): Workout[] {
    return this._workouts;
  }
  get selectedWorkouts(): Workout[] { 
    return this._selectedWorkouts;
  }

  get limitWorkouts(): Workout[] { 
    return this._limitWorkouts;
  }
  set limitWorkouts(workouts: Workout[]) { 
    this._limitWorkouts = workouts;
  }

  get error(): HttpErrorResponse | undefined {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  // Function to get all workouts
  public getAllWorkouts(): Subscription {
    this._loading = true;
    return this.http.get<Workout[]>(`${apiUrl}/workouts`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (workouts: Workout[]) => {
          this._workouts = workouts;
          this._selectedWorkouts = workouts;
          this._limitWorkouts = this._selectedWorkouts.slice(0,3);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to get a selected amount of workouts that have the given type
  public getSelectedWorkouts(type: string, pageSize: number): void {
    this._selectedWorkouts = [];
    this._limitWorkouts = [];

    if (type == "All") {
      this._selectedWorkouts = this._workouts;
    } else {
      for (let workout of this._workouts) {
        if (workout.type == type) {
          this._selectedWorkouts.push(workout);
        }
      }
    }

    this._limitWorkouts = this._selectedWorkouts.slice(0, pageSize);
  }

  // Function to complete a workout with given workoutId that belongs to given goalId
  completeWorkout(workoutId:number, goalId:number)
  {
    this._loading = true;
    return this.http.put(`${apiUrl}/workouts/${workoutId}/complete/`, {'goalId':goalId, 'complete': true})
    .pipe(
      finalize(() => {
        this._loading = false;
    }))
    .subscribe();
  }
  
  // Function to add a new workout
  createWorkout(userId:number, workoutName:string, workoutType:string, exerciseIds:number[])
  {
    this._loading = true;
    return this.http.post<Workout>(`${apiUrl}/workouts`, {
      'name': workoutName,
      'type': (workoutType == "" ? null : workoutType),
      'exerciseIds': exerciseIds,
      'userId': userId
    })
    .pipe(finalize(() => {
      this._loading = false;
    }))
    .subscribe({next: (workout) => {
        return workout;
    }, error: (error: HttpErrorResponse) => {
      this._error = error;
    }});
  }
  /*
      Function to edit an existing workout in the database. 
      Returns a Subscription so the components can add a finalizer if needed
  */
  editWorkout(workoutId:number, workout:Workout, exerciseIds:number[]) : Subscription
  {
    this._loading = true;
    return this.http.patch<Workout>(`${apiUrl}/workouts/${workoutId}`, {
      'id': workout.id,
      'name': workout.name,
      'type': workout.type,
      'exerciseIds': exerciseIds
    })
    .pipe(finalize(()=>{
      this._loading = false;
    }))
    .subscribe({next: (workout) => {
        return workout;
    }, error: (error: HttpErrorResponse) => {
      this._error = error;
    }});
  }
}
