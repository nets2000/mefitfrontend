import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { environment } from "../../environments/environment";
import {finalize, min, Observable} from "rxjs";
import { Exercise } from "../models/exercise.model";
import {MuscleGroup} from "../models/muscleGroup.model";

const { apiUrl } = environment;
@Injectable({
  providedIn: 'root'
})
export class ExercisesService {
  private _exercises: Exercise[] = [];
  private _userExercises: Exercise[] = [];
  private _goalExercises:Exercise[] = [];
  private _selectedExercises: Exercise[] = [];
  private _limitExercises: Exercise[] = this._exercises.slice(0,10);

  private _error?: HttpErrorResponse;
  private _loading: boolean = false;
  // private _muscleGroupId?: number;

  get exercises(): Exercise[] { return this._exercises }
  get goalExercises(): Exercise[] { return this._goalExercises; }
  get userExercises(): Exercise[] { return this._userExercises; }
  get selectedExercises(): Exercise[] { return this._selectedExercises }
  get error(): HttpErrorResponse | undefined { return this._error; }
  get loading(): boolean { return this._loading; }
  get limitExercises(): Exercise[] { return this._limitExercises }

  set selectedExercises(exercises: Exercise[]) { this._selectedExercises = exercises }
  set limitExercises(exercises: Exercise[]) { this._limitExercises = exercises }

  constructor(private readonly http: HttpClient) { }

  // Function to get all exercises in the database
  public findAllExercises(): void {
    this._loading = true;
    this.http.get<Exercise[]>(`${apiUrl}/exercise`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (exercises: Exercise[]) => {
          this._exercises = exercises;
          this._selectedExercises = exercises;
          this._limitExercises = exercises.slice(0,10);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to get all the muscle-groups for an exercise with the given id
  public findMuscleGroupsFromExercise(id: number): Observable<MuscleGroup[]> {
    return this.http.get<MuscleGroup[]>(`${apiUrl}/exercise/${id}/muscle-groups`);
  }

  // Function to get all exercises that belong to the muscle-group with the given musclegroupId
  public findExercisesWithMuscleGroup(muscleGroupId: number) {
    this._loading = true;
    this.http.get<Exercise[]>(`${apiUrl}/musclegroups/${muscleGroupId}/exercises`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (exercises: Exercise[]) => {
          this._selectedExercises = exercises;
          this._limitExercises = this._selectedExercises.slice(0,10);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to find an exercise in the database that belongs to the given goalId
  public findExercisesByGoalId(goalId: number)
  {
    this._loading = true;
    return this.http.get<Exercise[]>(`${apiUrl}/goals/${goalId}/exercises`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (exercises: Exercise[]) => {
          this._goalExercises = exercises;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to find an exercise in the database that belongs to the given userID
  public findExercisesByUserId(userId: number)
  {
    this._loading = true;
    return this.http.get<Exercise[]>(`${apiUrl}/users/${userId}/exercises-created`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (exercises: Exercise[]) => {
          this._userExercises = exercises;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to complete an exercise with given exerciseId and goalId
  public completeExercise(exerciseId:number, goalId:number)
  {
    this._loading = true;
    return this.http.put(`${apiUrl}/exercise/${exerciseId}/complete`, {'goalId':goalId, 'complete': true})
    .pipe(
      finalize(() => {
        this._loading = false;
      })
    )
    .subscribe({
      error: (error:HttpErrorResponse) => {
        this._error = error;
      }
    });
  }

  // Function to updated exercise
  public updateExercise(exercise:Exercise)
  {
    this._loading = true;
    return this.http.put(`${apiUrl}/exercise/${exercise.id}`, exercise)
    .pipe((finalize(() => {
      this._loading = false;
    })))
    .subscribe({
      error: (error:HttpErrorResponse) => {
        this._error = error;
      }
    });
  }
  
  // Function to create a new exercise
  public createNewExercise(exercise:Exercise)
  {
    this._loading = true;
    return this.http.post(`${apiUrl}/exercise`, exercise).pipe(finalize(() => {
      this._loading = false;
    }))
    .subscribe({
      error: (error:HttpErrorResponse) => {
        this._error = error;
      }
    });
  }
}
