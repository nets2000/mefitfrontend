import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Subscription } from 'rxjs';
import { Goal } from '../models/goals.model';
import { environment } from "../../../src/environments/environment";
import { Exercise } from '../models/exercise.model';
import { Workout } from '../models/workout.model';
import { ProfileManagerService } from './profile-manager.service';

const { apiUrl } = environment;

@Injectable({
  providedIn: 'root',
})
export class GoalService {
  private _currentGoals: Goal[] = [];
  private _previousGoals: Goal[] = [];
  private _createdGoal!: Goal;
  private _goalExercises: Exercise[] = [];
  private _goalWorkouts: Workout[] = [];
  private _loading: boolean = false;
  private _error?: HttpErrorResponse;

  get loading(): boolean {
    return this._loading;
  }
  get currentGoals(): Goal[] {
    return this._currentGoals;
  }
  get previousGoals(): Goal[] {
    return this._previousGoals;
  }
  get createdGoal(): Goal {
    return this._createdGoal;
  }
  get error(): HttpErrorResponse | undefined {
    return this._error;
  }
  get goalExercises(): Exercise[] {
    return this._goalExercises;
  }
  get goalWorkouts(): Workout[] {
    return this._goalWorkouts;
  }
  constructor(
    private http: HttpClient,
    private readonly profileService: ProfileManagerService
  ) { }

  public findExercisesByGoalId(goalId: number) {
    this._loading = true;
    return this.http.get<Exercise[]>(`${apiUrl}/goals/${goalId}/exercises`)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (exercises: Exercise[]) => {
          this._goalExercises = exercises;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  /*
  Function to find workouts of given goalId
  */
  public findWorkoutsByGoalId(goalId: number): Subscription {
    this._loading = true;
    this._goalWorkouts = [];
    return this.http.get<Workout[]>(`${apiUrl}/goals/${goalId}/workouts`)
      .pipe(finalize(() => {
        this._loading = false;
      }))
      .subscribe({
        next: (workouts) => {
          this._goalWorkouts = workouts;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to get past goals for user
  findHistory(): Subscription {
    this._loading = true;
    return this.http.get<Goal[]>(`${apiUrl}/profiles/${this.profileService.currentProfile?.id}/goals_history`)
      .pipe(
        finalize(() => {
          this._loading = false;
        }))
      .subscribe({
        next: (goals) => {
          goals.forEach(goal => {
            this._previousGoals.push(goal);
          });
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  /*
    Function to find the current users current goals
    Returns a Subscription so the components can add a finalizer if needed
  */
  findCurrent() {
    this._loading = true;
    return this.http.get<Goal[]>(`${apiUrl}/profiles/${this.profileService.currentProfile?.id}/goals`)
      .pipe(
        finalize(() => {
          this._loading = false;
        }))
      .subscribe({
        next: (goals) => {
          goals.forEach(goal => {
            // If goal wasn't achieved, check if date already passed
            if (!goal.achieved) {
              // User can no longer achieve this goal 
              if (this.datePassed(goal.endDate)) {
                this._previousGoals.push(goal);
                this.archiveGoal(goal);
              }
              // User can still achieve this goal
              else {
                this._currentGoals.push(goal);
              }
            }
            // Goal was already achieved
            else {
              this._previousGoals.push(goal);
            }
          });
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Checks if the enddate for the already passed so the goal can be archived
  datePassed(endDate: string): boolean {
    const dateObject = new Date();
    const year = dateObject.getFullYear(), month = dateObject.getMonth(), date = dateObject.getDate();
    const endDateSplit = endDate.split('T')[0].split('-');
    // Date format: yyyy-mm-dd
    if (parseInt(endDateSplit[0]) == year) {
      if (parseInt(endDateSplit[1]) < month)
        return true;
      else if (parseInt(endDateSplit[1]) == month + 1) {
        if (parseInt(endDateSplit[2]) < date)
          return true;
        else return false;
      }
      return false;
    }
    return true;
  }

  // Function to archive old goals in case they weren't achieved and the endDate has passed
  archiveGoal(goal: Goal) {
    this._loading = true;
    let date = new Date().toISOString().split('Z')[0];
    this.http.patch<Goal>(`${apiUrl}/goals/${goal.id}/achieve`, {
      'id': goal.id,
      'current': false,
      'achieved': false,
      'endDate': date,
      'profileId': this.profileService.GetProfileID()
    })
      .pipe(finalize(() => {
        this._loading = false;
      }))
      .subscribe({
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      });
  }

  // Creates a new goal with given end date and workout ids
  createGoal(endDate: string, workoutIds: number[], exerciseIds: number[], fitnessProgramId: number) {
    this._loading = true;
    return this.http.post<Goal>(`${apiUrl}/goals`,
      {
        "endDate": endDate,
        "workoutIds": workoutIds,
        "exerciseIds": exerciseIds,
        "profileId": this.profileService.GetProfileID(),
        "fitnessProgramId": (fitnessProgramId == -1 ? null : fitnessProgramId)
      })
      .pipe(finalize(() => {
        this._loading = false;
      }))
      .subscribe({
        next: (goal) => {
          this._createdGoal = goal;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
        }
      })
  }

  // Function to calculate the completed percentage of workouts and/or exercises
  completedPercentage(workouts: Workout[], exercises: Exercise[]): string {
    this._loading = true;
    let total = 0;
    let completed = 0;

    if (workouts) {
      workouts.forEach(workout => {
        if (workout.complete) completed++
        total++
      })
    }
    
    if (exercises) {
      exercises.forEach(exercise => {
        if (exercise.complete) completed++
        total++
      })
    }

    this._loading = false;
    // Only calculate completed workouts if there are workouts and/or exercises (no divide by 0)
    if (total != 0) {
      let percentage = completed / total * 100;
      return percentage.toFixed(2);
    }
    // Goal doesn't have any workouts or exercises
    return '0';
  }
}
