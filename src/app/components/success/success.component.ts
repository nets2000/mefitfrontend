import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-success',
  templateUrl: './success.component.html'
})
export class SuccessComponent implements OnInit {
  @Input() item: string = " element";
  @Input() update:boolean = false;
  constructor(private router:Router) { }

  ngOnInit(): void {
    Swal.fire({
      icon: 'success',
      title: 'Success',
      text: `Successfully ${this.update ? 'updated' : 'created'} ${this.item}.`,
      customClass: {
        closeButton: 'button',
        confirmButton: 'button'
      },
      confirmButtonText: 'Close',
      buttonsStyling: false,
      timer: 5000,
      timerProgressBar: true
    })
  }

}
