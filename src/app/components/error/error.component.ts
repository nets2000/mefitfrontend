import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-error',
  templateUrl: './error.component.html'
})
export class ErrorComponent implements OnInit {
  @Input() error!:HttpErrorResponse | undefined;
  message:string = "";
  redirect:boolean = false;
  constructor(private route:Router) { }

  ngOnInit(): void {
    this.setMessage();
    // SweetAlert custom pop up
    Swal.fire({
      icon: 'error',
      customClass: {
        closeButton: 'button',
        confirmButton: 'button'
      },
      buttonsStyling: false,
      title: `${this.error?.status} ${this.error?.statusText}`,
      text: this.message
    });
    if (this.error?.status == 404 || this.error?.status == 401)
    {
      this.redirect = true;
      setTimeout(() => {
        this.route.navigateByUrl('/');
      }, 5000)
    }
    else {
      this.redirect = false;
    }
  }
  private setMessage():void{
    if (this.error)
    {
      switch (this.error?.status)
      {
        case (400):
          this.message = "The information you provided was in an incorrect format. Please try again. "
          break;
        case (401):
          this.message = "You are not authorized to perform this action."
          break;
        case (404):
          this.message = "The requested resource was not found. "
          break;
        case (500):
          this.message = "An internal server error occurred while processing your request. Please make sure you provided the correct information, or contact the system administrator if the problem persits."
          break;
        default:
          this.message = "An unexpected error occured while processing your request."
          break;
      }
    }
    else {
      this.message = "An unexpected error occured while processing your request."
    }
  }

}
