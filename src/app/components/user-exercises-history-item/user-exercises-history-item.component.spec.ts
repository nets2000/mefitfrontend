import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserExercisesHistoryItemComponent } from './user-exercises-history-item.component';

describe('UserExercisesHistoryItemComponent', () => {
  let component: UserExercisesHistoryItemComponent;
  let fixture: ComponentFixture<UserExercisesHistoryItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserExercisesHistoryItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserExercisesHistoryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
