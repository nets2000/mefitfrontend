import { Component, Input, OnInit } from '@angular/core';
import { Exercise } from 'src/app/models/exercise.model';

@Component({
  selector: 'app-user-exercises-history-item',
  templateUrl: './user-exercises-history-item.component.html',
  styleUrls: ['./user-exercises-history-item.component.scss']
})
export class UserExercisesHistoryItemComponent implements OnInit {
  @Input() exercise!:Exercise;
  private _edit:boolean = false;
  get edit():boolean { return this._edit; }
  constructor() { }

  ngOnInit(): void {
  }
  changeEdit():void {
    this._edit = !this._edit;
  }

}
