import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusclegroupsChecklistComponent } from './musclegroups-checklist.component';

describe('MusclegroupsChecklistComponent', () => {
  let component: MusclegroupsChecklistComponent;
  let fixture: ComponentFixture<MusclegroupsChecklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusclegroupsChecklistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MusclegroupsChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
