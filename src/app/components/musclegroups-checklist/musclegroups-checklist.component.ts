import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MuscleGroup } from 'src/app/models/muscleGroup.model';

@Component({
  selector: 'app-musclegroups-checklist',
  templateUrl: './musclegroups-checklist.component.html',
  styleUrls: ['./musclegroups-checklist.component.scss']
})
export class MusclegroupsChecklistComponent implements OnInit {
  @Input() muscleGroups!:MuscleGroup[];
  @Input() selectedMuscleGroups?:number[];
  @Input() editExisting?:boolean;
  @Output() changedMuscleGroups:EventEmitter<number> = new EventEmitter<number>()
  enableEdit:boolean = true;

  constructor() { }

  ngOnInit(): void {
    if (this.selectedMuscleGroups && this.selectedMuscleGroups.length != 0 && this.editExisting)
    {
      this.enableEdit = false;
    }
  }
  editMuscleGroups()
  {
    this.enableEdit = !this.enableEdit;
  }
  changeMuscleGroups(muscleGroupId:number)
  {
    this.changedMuscleGroups.emit(muscleGroupId);
  }

}
