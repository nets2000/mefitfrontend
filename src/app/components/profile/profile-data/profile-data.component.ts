import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address } from 'src/app/models/address.model';
import { ProfilePic } from 'src/app/models/profile-pic.model';
import { Profile } from 'src/app/models/profile.model';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-profile-data',
  templateUrl: './profile-data.component.html',
  styleUrls: ['./profile-data.component.scss']
})
export class ProfileDataComponent {

  @Output() editProfile: EventEmitter<void> = new EventEmitter();
  @Input() imgURL: string = "";

  get fullName(): string {
    return keycloak.tokenParsed?.name!;
  }

  get email(): string {
    return keycloak.tokenParsed?.email!;
  }

  get profile(): Profile {
    return this.profileManager.currentProfile!;
  }

  get address(): Address {
    return this.profileManager.currentAddress!;
  }

  // True if a user is a contributor or a admin else false
  get isContributor(): boolean {
    if (this.profileManager.currentUser?.isContributor || this.profileManager.currentUser?.isAdmin) {
      return true;
    }
    return false;
  }

  // Replaces the . with a , for the number format
  get weight(): string {
    return this.profile.weight.toString().replace(".", ",");
  }

  // Replaces the . with a , and adds an extra 0 for proper formatting
  get height(): string {
    let height: string = this.profile.height.toString();
    if (height.length < 4) {
      height += "0";
    }
    return height.replace(".", ",");
  }

  constructor(
    private readonly profileManager: ProfileManagerService
  ) { }

  // Goes to the edit profile view
  public EnableEditingProfile(): void {
    this.editProfile.emit();
  }

  // Goes to the account management of keycloak
  public EditAccountInfo(): void {
    keycloak.accountManagement();
  }
}
