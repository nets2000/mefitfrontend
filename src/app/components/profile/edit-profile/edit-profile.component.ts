import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Address, AddressCreate } from 'src/app/models/address.model';
import { Profile, ProfileUpdate } from 'src/app/models/profile.model';
import { ProfilePage } from 'src/app/pages/profile/profile.page';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import { ProfileFormComponent } from '../profile-form/profile-form.component';
import { environment } from "../../../../environments/environment";
import keycloak from 'src/keycloak';
import { User, UserUpdate } from 'src/app/models/user.model';
import { ProfilePic, ProfilePicUpdate } from 'src/app/models/profile-pic.model';
import { DisabilityType } from 'src/app/enums/disabilities.enum';
import { Countries } from 'src/app/enums/countries.enum';
import { Workout } from 'src/app/models/workout.model';
import { Goal } from 'src/app/models/goals.model';
import { FormBuilder, Validators } from '@angular/forms';

const { apiUrl } = environment;

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent {

  @Output() stopEditProfile: EventEmitter<void> = new EventEmitter();
  @Output() reloadProfilePicture: EventEmitter<void> = new EventEmitter();
  // The image that can be edited
  @Input() imgURL: string = "";

  private _profileForm: ProfileFormComponent | undefined = undefined;

  private _editingProfile: boolean = false;

  get profile(): Profile {
    return this.profileManager.currentProfile!;
  }

  get address(): Address {
    return this.profileManager.currentAddress!;
  }

  get wantsToBeContributor(): boolean {
    return this.profileManager.currentUser!.wantsToBeContributor;
  }

  get editingProfile(): boolean {
    return this._editingProfile;
  }

  get validForm(): boolean {
    return this._profileForm!.GetIfProfileFormIsValid();
  }

  constructor(
    private readonly profileManager: ProfileManagerService,
    private readonly http: HttpClient
  ) { }

  // Start saving changes to profile
  public SaveChangesToProfile(): void {
    this._editingProfile = true;

    // Check whether user still has the base image if so, use null as the value for the updated profile pic
    if (this._profileForm!.imgURL === this._profileForm!.baseImage) {
      this.UpdateUser(null);
    } else {
      // If the user selected a new file create an profile pic update object
      if (this._profileForm!.selectedFile) {
        let updatedProfilePic: ProfilePicUpdate = {
          name: this._profileForm!.selectedFile!.name,
          type: this._profileForm!.selectedFile!.type,
          pic: this.ConvertImageUrlToBase64(this._profileForm!.imgURL)
        }

        // If the user already had chosen a profile pic before, update it to the new info
        if (this.profileManager.currentUser!.profilePicId) {
          this.http.put<ProfilePic>(`${apiUrl}/profilepics/${this.profileManager.currentUser!.profilePicId}`, updatedProfilePic)
            .subscribe({
              next: (profilePic: ProfilePic) => {
                // Update user with same profile pic id
                this.UpdateUser(this.profileManager.currentUser!.profilePicId!);
              },
              error: (error: HttpErrorResponse) => {
                console.log(error.message)
              }
            })
        } else {
          // If the user hadn't chosen a profile pic before, create a new entry in the database
          this.http.post<ProfilePic>(`${apiUrl}/profilepics`, updatedProfilePic)
            .subscribe({
              next: (profilePic: ProfilePic) => {
                // Set profile pic to user
                this.UpdateProfilePic(profilePic.id);
              },
              error: (error: HttpErrorResponse) => {
                console.log(error.message)
              }
            })
        }
      } else {
        // If the user already had a costum profile pic but didn't change it, go straight to update user with old profile pic info
        this.UpdateUser(this.profileManager.currentUser!.profilePicId);
      }
    }
  }

  // Updates the profile pic id of the user to the given profile pic id
  private UpdateProfilePic(profilePicId: number) {
    this.http.put<any>(`${apiUrl}/users/${this.profileManager.currentUser!.id}/profilepic?profilePicId=${profilePicId}`, profilePicId)
      .subscribe({
        next: (user) => {
          this.UpdateUser(profilePicId);
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Updates the users wants to be contributor status by creating a user update object
  private UpdateUser(profilePicId: number | null): void {
    let wantsToBeContributor: string = this._profileForm!.profileForm.get('wantsToBeContributor')!.value!;
    if (wantsToBeContributor == "") {
      wantsToBeContributor = String(this._profileForm!.initialWantsToBeContributor);
    }

    let updatedUser: UserUpdate = {
      firstName: keycloak.tokenParsed!.given_name!,
      lastName: keycloak.tokenParsed!.family_name!,
      wantsToBeContributor: wantsToBeContributor,
      isContributor: String(this.profileManager.currentUser!.isContributor),
      isAdmin: String(this.profileManager.currentUser!.isAdmin)
    }

    // Update user in program
    this.profileManager.UpdateCurrentUser(updatedUser, profilePicId);

    // Update user in database
    this.http.put<any>(`${apiUrl}/users/${this.profileManager.currentUser!.id}`, updatedUser)
      .subscribe({
        next: (user) => {
          this.UpdateAddress();
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Update address of the user with the given information by creating an address create object
  private UpdateAddress(): void {
    let country: string = this._profileForm!.profileForm.get('country')!.value!;
    if (country == "") {
      country = Object.values(Countries)[0];
    }

    let updatedAddress: AddressCreate = {
      addressLine1: this._profileForm!.profileForm.get('addressLine1')!.value!,
      addressLine2: this._profileForm!.profileForm.get('addressLine2')!.value!,
      addressLine3: this._profileForm!.profileForm.get('addressLine3')!.value!,
      country: country,
      city: this._profileForm!.profileForm.get('city')!.value!,
      postalCode: this._profileForm!.profileForm.get('postalCode')!.value!,
      profileIds: []
    }

    // Update the address in the program
    this.profileManager.UpdateCurrentAddress(updatedAddress);

    // We cant just update the current address of the user, because multiple people might have the same address and we don't want to change all their addresses as well
    // Check whether the newly given address exists
    this.http.get<Address>(`${apiUrl}/addresses/${updatedAddress.postalCode}/adressbydata?city=${updatedAddress.city}&street=${updatedAddress.addressLine1}&houseNumber=${updatedAddress.addressLine2}`)
      .subscribe({
        next: (address: Address) => {
          // Update the address of the user to the found address
          this.UpdateAddressOfProfile(address.id);
        },
        error: (error: HttpErrorResponse) => {
          if (error.status == 404) {
            // If not found create a new address
            this.CreateNewAddress(updatedAddress);
          } else {
            console.error(error.message);
          }
        }
      })
  }

  // Creates a new address with the address create object information
  private CreateNewAddress(newAddress: AddressCreate): void {
    this.http.post<Address>(`${apiUrl}/addresses`, newAddress)
      .subscribe({
        next: (address: Address) => {
          // Use that information to update the address of the profile
          this.UpdateAddressOfProfile(address.id);
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Updates the address id of the profile to the given address id
  private UpdateAddressOfProfile(addressId: number) {
    this.http.put<any>(`${apiUrl}/profiles/${this.profileManager.currentProfile!.id}/address?addressId=${addressId}`, addressId)
      .subscribe({
        next: (user) => {
          this.GetWorkoutsOfProfile();
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Get all workout ids of the profile, these are necessary for the profile update object
  private GetWorkoutsOfProfile(): void {
    this.http.get<Workout[]>(`${apiUrl}/profiles/${this.profileManager.currentProfile!.id}/workouts`)
      .subscribe({
        next: (workouts: Workout[]) => {
          let workoutIds: number[] = [];
          workouts.forEach(w => workoutIds.push(w.id));
          this.GetGoalsOfProfile(workoutIds);
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

    // Get all goal ids of the profile, these are necessary for the profile update object
  private GetGoalsOfProfile(workoutIds: number[]): void {
    this.http.get<Goal[]>(`${apiUrl}/profiles/${this.profileManager.currentProfile!.id}/goals`)
      .subscribe({
        next: (goals: Goal[]) => {
          let goalIds: number[] = [];
          goals.forEach(g => goalIds.push(g.id));
          this.UpdateProfile(workoutIds, goalIds);
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Create an profile update object with all given information and update the profile
  private UpdateProfile(workoutIds: number[], goalIds: number[]): void {
    let disabilities: string = this.GetStringValueFromPossibilities(this._profileForm!.profileForm.get('disabilities')!.value!, this._profileForm!.disabilityTypes[0],
      this._profileForm!.profileForm.get('otherDisabilities')!.value!);

    let medicalConditions: string = this.GetStringValueFromPossibilities(this._profileForm!.profileForm.get('medicalConditions')!.value!, this._profileForm!.medicalConditions[0],
      this._profileForm!.profileForm.get('otherMedicalConditions')!.value!);

    let updatedProfile: ProfileUpdate = {
      height: Number.parseFloat(this._profileForm!.profileForm.get('height')!.value!.replace(',', '.')),
      weight: Number.parseFloat(this._profileForm!.profileForm.get('weight')!.value!.replace(',', '.')),
      disabilities: disabilities,
      medicalConditions: medicalConditions,
      fitnessEvaluation: this.profileManager.currentProfile!.fitnessEvaluation,
      workoutIds: workoutIds,
      goalIds: goalIds
    }

    // Update profile in program
    this.profileManager.UpdateCurrentProfile(updatedProfile);

    // Update profile in database
    this.http.put<any>(`${apiUrl}/profiles/${this.profileManager.currentProfile!.id}`, updatedProfile)
      .subscribe({
        next: (profile) => {
          this.reloadProfilePicture.emit();
          this.stopEditProfile.emit();
          this._editingProfile = false;
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // If no other option is selected from the dropdown the string is empty and thus the option from the dropdown is the first option
  // If the option selected is the Other... option then get the input from the input field
  private GetStringValueFromPossibilities(formField: string, firstOptionFromDropdown: string, otherInputField: string): string {
    let dropdownField: string = formField;
    if (dropdownField == "") {
      dropdownField = firstOptionFromDropdown;
    } else if (dropdownField == DisabilityType.Other) {
      dropdownField = otherInputField;
    }

    return dropdownField;
  }

  // Converts the image url to a base 64 string
  private ConvertImageUrlToBase64(imageURL: string): string {
    var base64Index = imageURL.indexOf(';base64,') + ';base64,'.length;
    return imageURL.substring(base64Index);
  }

  // Gets the profile form
  public GetProfileForm(profileForm: ProfileFormComponent) {
    this._profileForm = profileForm;
  }
}
