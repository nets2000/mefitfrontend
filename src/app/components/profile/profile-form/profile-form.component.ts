import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Countries } from 'src/app/enums/countries.enum';
import { DisabilityType } from 'src/app/enums/disabilities.enum';
import { MedicalConditions } from 'src/app/enums/medical-conditions.enum';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @Output() setProfileForm: EventEmitter<ProfileFormComponent> = new EventEmitter();
  // All input that a profile form has can be pre set
  @Input() initialWeight: number = -1;
  @Input() initialHeight: number = -1;
  @Input() initialDisability: string = "";
  @Input() initialMedicalCondition: string = "";
  @Input() initialAddressLine1: string = "";
  @Input() initialAddressLine2: string = "";
  @Input() initialAddressLine3: string = "";
  @Input() initialCity: string = "";
  @Input() initialCountry: string = "";
  @Input() initialPostalCode: string = "";
  @Input() initialWantsToBeContributor: boolean = false;
  @Input() initialImageURL: string = "";

  public profileForm = this.builder.group({
    weight: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(6), Validators.pattern("^([0-9]+(\,?[0-9]?[0-9]?)?)")]],
    height: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern("^([0-9]+(\,[0-9][0-9]))")]],
    disabilities: [''],
    otherDisabilities: [''],
    medicalConditions: [''],
    otherMedicalConditions: [''],
    addressLine1: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100), Validators.pattern("^[A-Za-z\ ]*$")]],
    addressLine2: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(4), Validators.pattern("^[0-9]+$")]],
    addressLine3: ['', [Validators.minLength(1), Validators.maxLength(4)]],
    city: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern("^[A-Za-z\ ]*$")]],
    country: [''],
    postalCode: ['', [Validators.required]],
    wantsToBeContributor: ['']
  });

  private _disabilityTypes: string[] = [];
  private _medicalConditions: string[] = [];
  private _countries: string[] = [];

  private _selectedFile: File | undefined;
  private _baseImage: string = "../../../../assets/baseProfilePic.jpg";
  private _imgURL: string = this._baseImage;

  constructor(
    private readonly builder: FormBuilder
  ) { }

  get disabilityTypes(): string[] {
    return this._disabilityTypes;
  }

  get medicalConditions(): string[] {
    return this._medicalConditions;
  }

  get countries(): string[] {
    return this._countries;
  }

  get imgURL(): string {
    return this._imgURL;
  }

  get otherDisabilityType(): string {
    return DisabilityType.Other;
  }

  get otherMedicalCondition(): string {
    return MedicalConditions.Other;
  }

  get selectedFile(): File | undefined {
    return this._selectedFile;
  }

  get baseImage(): string {
    return this._baseImage;
  }

  // Set profile form data to preset data if it has preset data, also fill the dropdowns with data
  ngOnInit(): void {
    // Sets a profile form in a object
    this.setProfileForm.emit(this);

    const disabilityTypes = Object.values(DisabilityType);
    this.SetGivenValueInDropdown(this.initialDisability, disabilityTypes, this._disabilityTypes, 'disabilities', this.otherDisabilityType, "otherDisabilities");

    const medicalConditions = Object.values(MedicalConditions);
    this.SetGivenValueInDropdown(this.initialMedicalCondition, medicalConditions, this._medicalConditions, "medicalConditions", this.otherMedicalCondition,
      "otherMedicalConditions");

    const countries = Object.values(Countries);
    this.SetGivenValueInDropdown(this.initialCountry, countries, this._countries);

    this.SetGivenValueInInputField(this.initialWeight.toString().replace(".", ","), 'weight');
    this.SetGivenValueInInputField(this.FormatHeight(), 'height');
    this.SetGivenValueInInputField(this.initialAddressLine1, 'addressLine1');
    this.SetGivenValueInInputField(this.initialAddressLine2, 'addressLine2');
    this.SetGivenValueInInputField(this.initialAddressLine3, 'addressLine3');
    this.SetGivenValueInInputField(this.initialCity, 'city');
    this.SetGivenValueInInputField(this.initialPostalCode, 'postalCode');

    // Check want to be a contributor if initial was true
    if (this.initialWantsToBeContributor) {
      this.profileForm.get("wantsToBeContributor")?.setValue(this.initialWantsToBeContributor.toString());
    }

    if (this.initialImageURL != "") {
      this._imgURL = this.initialImageURL;
    }
  }

  // Format height to the correct string format
  private FormatHeight() : string {
    let height: string = this.initialHeight.toString().replace(".", ",");
    if (height.length < 4 && height != "-1") {
      height += 0;
    }
    
    return height;
  }

  // If initial value then set the input value to initial value
  private SetGivenValueInInputField(initialValue: string, inputField: string): void {
    if (initialValue != "" && initialValue != "-1") {
      this.profileForm.get(inputField)!.setValue(initialValue);
    }
  }

  // Set first option of the array of the dropdown to initial value if there was one. 
  // If initial value does not exist as one of the options then activate the input field with the given input value and set value of dropdown to 'Other...'
  // Always fill the dropdown array with all the given values
  private SetGivenValueInDropdown(initialValue: string, allAddedValues: string[], arrayToAddValuesTo: string[], dropdown: string = "", otherInputOfDropdown: string = "",
    inputFieldOfDropdown: string = ""): void {
    if (initialValue != "") {
      if (allAddedValues.includes(initialValue)) {
        arrayToAddValuesTo.push(initialValue);
      } else {
        arrayToAddValuesTo.push(otherInputOfDropdown);
        this.profileForm.get(dropdown)!.setValue(otherInputOfDropdown);
        this.profileForm.get(inputFieldOfDropdown)!.setValue(initialValue);
      }
    }

    allAddedValues.forEach((value, index) => {
      if (!arrayToAddValuesTo.includes(value)) {
        arrayToAddValuesTo.push(value);
      }
    });
  }

  // Set the imgURL equal to that of the uploaded picture
  public UploadProfilePicture(event: any) {
    this._selectedFile = event.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this._imgURL = reader.result! as string;
    };
  }

  // Checks whether the all values that are given are valid
  public GetIfProfileFormIsValid(): boolean {
    if (this.profileForm.valid &&
      this.CheckIfCorrectlyFilledIn(this.profileForm.get('disabilities')!.value, this.profileForm.get('otherDisabilities')!.value, this.otherDisabilityType) &&
      this.CheckIfCorrectlyFilledIn(this.profileForm.get('medicalConditions')!.value, this.profileForm.get('otherMedicalConditions')!.value, this.otherMedicalCondition)
      && this.CheckIfImageIsValid()) {
      return true;
    }

    return false;
  }

  // Checks whether a string is equal to a value
  public CheckIfStringIsValue(value: string | null, other: string): boolean {
    return Boolean(value === other);
  }

  // Checks whether a string contains 0 characters
  public CheckIfStringIsFilledIn(value: string | null): boolean {
    return Boolean(value!.length === 0);
  }

  // Checks whether a string has at least 3 characters
  public CheckIfStringHasMinimumValue(value: string | null): boolean {
    return Boolean(value!.length < 3 && value!.length > 0);
  }

  // Checks whether the uploaded file of the profile picture is an image 
  public CheckIfImageIsValid(): boolean {
    if (!this._selectedFile) {
      return true;
    } else {
      if (this._selectedFile.type.split('/')[0] === 'image') {
        return true;
      } else {
        return false;
      }
    }
  }

  // Checks whether the input field of a dropdown is correctly filled in
  public CheckIfCorrectlyFilledIn(initial: string | null, value: string | null, other: string): boolean {
    if (this.CheckIfStringIsValue(initial, other)) {
      if (this.CheckIfStringIsFilledIn(value) || this.CheckIfStringHasMinimumValue(value)) {
        return false;
      } else {
        return true;
      }
    }

    return true;
  }
}
