import { Component, OnInit } from '@angular/core';
import { Workout } from 'src/app/models/workout.model';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import { UsersService } from 'src/app/services/users.service';
@Component({
  selector: 'app-user-workouts-history',
  templateUrl: './user-workouts-history.component.html',
  styleUrls: ['./user-workouts-history.component.scss']
})
export class UserWorkoutsHistoryComponent implements OnInit {
  private _edit:boolean = false;
  get loading():boolean { return this.usersService.loading; }
  get edit():boolean { return this._edit; }
  get error() { return this.usersService.error; }
  get workouts():Workout[] { return this.usersService.workoutsCreated; }
  constructor(private usersService:UsersService) { }

  ngOnInit(): void {
    this.usersService.getPastUserWorkoutCreations();
  }
  changeEdit(){
    this._edit = !this._edit;
  }
}
