import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWorkoutsHistoryComponent } from './user-workouts-history.component';

describe('UserWorkoutsHistoryComponent', () => {
  let component: UserWorkoutsHistoryComponent;
  let fixture: ComponentFixture<UserWorkoutsHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserWorkoutsHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserWorkoutsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
