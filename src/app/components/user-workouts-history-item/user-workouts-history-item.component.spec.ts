import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWorkoutsHistoryItemComponent } from './user-workouts-history-item.component';

describe('UserWorkoutsHistoryItemComponent', () => {
  let component: UserWorkoutsHistoryItemComponent;
  let fixture: ComponentFixture<UserWorkoutsHistoryItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserWorkoutsHistoryItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserWorkoutsHistoryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
