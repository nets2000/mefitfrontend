import { Component, Input, OnInit } from '@angular/core';
import { Workout } from 'src/app/models/workout.model';

@Component({
  selector: 'app-user-workouts-history-item',
  templateUrl: './user-workouts-history-item.component.html',
  styleUrls: ['./user-workouts-history-item.component.scss']
})
export class UserWorkoutsHistoryItemComponent implements OnInit {
  @Input() workout!:Workout;
  private _edit:boolean = false;
  get edit():boolean { return this._edit; }
  constructor() { }

  ngOnInit(): void {
  }
  changeEdit(){
    this._edit = !this._edit;
  }

}
