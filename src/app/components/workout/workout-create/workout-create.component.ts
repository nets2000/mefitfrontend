import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Exercise } from 'src/app/models/exercise.model';
import { Workout } from 'src/app/models/workout.model';
import { MuscleGroup } from 'src/app/models/muscleGroup.model';
import { GoalService } from 'src/app/services/goals.service';
import { WorkoutsService } from 'src/app/services/workouts.service';
import { MuscleGroupService } from 'src/app/services/muscle-group.service';
import { ExercisesService } from 'src/app/services/exercises.service';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import { NgForm } from '@angular/forms';
import { Profile } from 'src/app/models/profile.model';
import { WorkoutTypes } from 'src/app/enums/workout-types.enum';
import { ActivatedRoute } from '@angular/router';
import { WorkoutDetailService } from 'src/app/services/workout-detail.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
    selector: 'app-workout-create',
    templateUrl: './workout-create.component.html',
    styleUrls: ['./workout-create.component.scss']
})
export class WorkoutCreateComponent implements OnInit {
    @ViewChild('paginator') paginator?: MatPaginator;
    @Input() editWorkout?: Workout;
    private _exerciseIds: number[] = []
    private _pageSize: number = 10;

    private _pageSizeOptions: number[] = [10, 25, 100];
    private _allWorkoutTypes: string[] = [];
    selectedMuscleGroup: number = 0;

    private workoutId!: number;
    private success_: boolean = false;
    private workout_creating_: boolean = false;
    private workout_!: Workout;
    private _loading: boolean = false;
    private _enableName: boolean = false;

    get success(): boolean { return this.success_; }
    get workout_creating(): boolean { return this.workout_creating_; }
    get workouts(): Workout[] { return this.workoutService.workouts; }
    get exercises(): Exercise[] { return this.exercisesService.exercises; }
    get workoutExercises(): Exercise[] { return this.workoutDetailService.workoutExercises; }
    get loading(): boolean { return this.goalService.loading || this.workoutService.loading || this.muscleGroupService.loading || this._loading; }
    get workout() { return this.workout_; }
    get selectedExercises(): Exercise[] {
        return this.exercisesService.selectedExercises;
    }

    get limitExercises(): Exercise[] {
        return this.exercisesService.limitExercises;
    }
    get muscleGroups(): MuscleGroup[] {
        return this.muscleGroupService.muscleGroups;
    }

    get error(): HttpErrorResponse | undefined {
        return this.workoutService.error;
    }

    get pageSizeOptions() {
        return this._pageSizeOptions;
    }

    get allWorkoutTypes(): string[] {
        return this._allWorkoutTypes;
    }

    get exerciseIds(): number[] {
        return this._exerciseIds;
    }
    get enableName(): boolean {
        return this._enableName;
    }

    public CheckIfAddedExercises(): boolean {
        if (this.exerciseIds.length > 0) {
            return true;
        }
        return false;
    }

    constructor(
        private goalService: GoalService,
        private workoutService: WorkoutsService,
        private workoutDetailService: WorkoutDetailService,
        private exercisesService: ExercisesService,
        private muscleGroupService: MuscleGroupService,
        private profileManagerService: ProfileManagerService,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        const workoutTypes = Object.values(WorkoutTypes);
        this._exerciseIds = [];
        workoutTypes.forEach((value) => {
            this._allWorkoutTypes.push(value);
        });
        this._loading = true;
        this.workoutService.getAllWorkouts().add(() => {
            // If we are editing a workout
            if (this.editWorkout != undefined) {
                this.workout_ = this.editWorkout;
                this.workoutDetailService.findExercisesFromWorkout(this.workoutId).add(() => {
                    this.workoutDetailService.workoutExercises.forEach(exercise => {
                        if (exercise.id)
                            this._exerciseIds.push(exercise.id);
                    });
                    this._loading = false;
                });
            }
        });
        this.exercisesService.findAllExercises();
        if (this.muscleGroups.length == 0) this.muscleGroupService.findAllMuscleGroups();
        setTimeout(() => {
            this._loading = false;
        }, (1000));
    }

    // Form submit function 
    public workoutCreateSubmit(workoutCreateForm: NgForm) {
        this.workout_creating_ = true;
        const { workoutName, workoutType } = workoutCreateForm.value;
        if (this.profileManagerService.currentProfile) {
            if (this.workout == undefined) {
                this.workoutService.createWorkout(this.profileManagerService.currentProfile.userId, workoutName, workoutType, this.exerciseIds)
                    .add(() => {
                        if (this.error) {
                            this.success_ = false;
                        }
                        else {

                            this.success_ = true;
                        }
                        this.workout_creating_ = false;
                    });
            }
            else {
                if (workoutName != undefined)
                    this.workout.name = workoutName;
                if (workoutType != undefined)
                    this.workout.type = workoutType;
                this.workoutService.editWorkout(this.workout.id, this.workout, this.exerciseIds).add(() => {
                    this.success_ = true;
                    this.workout_creating_ = false;
                })
            }
        }
        else {
            this.workout_creating_ = false;
            this.success_ = false;
        }
    }
    public reset() {
        this._exerciseIds = [];
    }
    // Listens for click on checkbox of exercise
    changeExercises(exerciseId: any) {
        if (!this.isChecked(exerciseId)) {
            this._exerciseIds.push(exerciseId)
        }
        else {
            this._exerciseIds.splice(this._exerciseIds.indexOf(exerciseId), 1)
        }
    }

    // Checks if checkbox is currently selected
    public isChecked(id: number) {
        return this._exerciseIds.indexOf(id) != -1;
    }
    public enableNameEdit() {
        this._enableName = !this._enableName;
    }

    //#region PAGINATION SETTINGS 
    // Pagination settings
    changeMuscleGroup(e: any) {
        this.selectedMuscleGroup = e.target.value;
        if (this.selectedMuscleGroup == 0) {
            this.exercisesService.selectedExercises = this.exercisesService.exercises;
        } else {
            this.exercisesService.findExercisesWithMuscleGroup(this.selectedMuscleGroup);
        }

        this.paginator?.firstPage();
        this.SetPageContent(0);
    }

    OnPageChange(event: PageEvent) {
        this.exercisesService.limitExercises = [];
        const startIndex = event.pageIndex * this._pageSize;

        this.SetPageContent(startIndex);
    }

    private SetPageContent(startIndex: number) {
        let endIndex = startIndex + this._pageSize;
        if (endIndex > this.selectedExercises.length) {
            endIndex = this.selectedExercises.length;
        }
        this.exercisesService.limitExercises = this.selectedExercises.slice(startIndex, endIndex);
    }

    public ChangePageSizeEvent(e: any) {
        this._pageSize = Number.parseInt(e.target.value);
        this.ChangePageSize(this._pageSize);
    }

    private ChangePageSize(pageSize: number) {
        this.paginator?._changePageSize(pageSize);
        this.paginator?.firstPage();
    }
    //#endregion
}
