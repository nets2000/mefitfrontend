import {Component, Input, OnInit} from '@angular/core';
import {Exercise} from "src/app/models/exercise.model";
import {Workout} from "src/app/models/workout.model";
import {WorkoutDetailService} from "src/app/services/workout-detail.service";

@Component({
  selector: 'app-workout-detail',
  templateUrl: './workout-detail.component.html',
  styleUrls: ['./workout-detail.component.scss']
})
export class WorkoutDetailComponent implements OnInit {

  // The information of the selected workout
  @Input() selectedWorkout!: Workout;
  private _exercises: Exercise[] = [];

  get exercises(): Exercise[] { return this._exercises }

  set exercises(newExercises: Exercise[]){ 
    this._exercises = newExercises;
  }

  constructor(private readonly workoutDetailService: WorkoutDetailService ) { }

  // Get the information of the selected workout
  ngOnInit(): void {
    this.workoutDetailService.findExercisesFromWorkout(this.selectedWorkout.id, this);
  }
}
