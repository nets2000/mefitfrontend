import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Exercise } from 'src/app/models/exercise.model';
import { Workout } from 'src/app/models/workout.model';
import { WorkoutDetailService } from 'src/app/services/workout-detail.service';

@Component({
  selector: 'app-workout-checklist',
  templateUrl: './workout-checklist.component.html',
  styleUrls: ['./workout-checklist.component.scss']
})
export class WorkoutChecklistComponent implements OnInit {
  @Input() workout!:Workout;
  @Input() checked!:boolean;
  @Input() selectedWorkouts!:number[];
  @Output() changedWorkouts: EventEmitter<number> = new EventEmitter<number>();
  get exercises():Exercise[] {return this.workoutExercises; }
  get loading():boolean { return this.workoutDetailService.loading; }
  get showExercises():boolean { return this.showExercises_; }
  private showExercises_:boolean = false;
  private workoutExercises:Exercise[] = [];
  constructor(private workoutDetailService:WorkoutDetailService) { }

  ngOnInit(): void {}
  workoutCheckboxChange(workoutId:number)
  {
    this.changedWorkouts.emit(workoutId);
  }
  displayExercises()
  {
    if (this.showExercises)
    {
      this.showExercises_ = false;
    }
    else {
      this.showExercises_ = true;
      this.workoutDetailService.findExercisesFromWorkout(this.workout.id).add(() => {
        this.workoutExercises = this.workoutDetailService.workoutExercises;
      })
    }
  }

}
