import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutChecklistComponent } from './workout-checklist.component';

describe('WorkoutChecklistComponent', () => {
  let component: WorkoutChecklistComponent;
  let fixture: ComponentFixture<WorkoutChecklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutChecklistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkoutChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
