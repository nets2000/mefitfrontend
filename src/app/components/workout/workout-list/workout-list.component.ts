import { Component, EventEmitter, Input, Output } from '@angular/core';
import {Workout} from "src/app/models/workout.model";
import { FitnessProgramService } from 'src/app/services/fitness-program.service';

@Component({
  selector: 'app-workout-list',
  templateUrl: './workout-list.component.html',
  styleUrls: ['./workout-list.component.scss'],
  exportAs:'cdkAccordionItem',
})
export class WorkoutListComponent {
  // Get if the button of the workout being added/ removed from a workout needs to be shown
  @Input() canAddToProgram: boolean = false;
  // All the workouts that need to be displayed
  @Input() workouts: Workout[] = [];
  // Get if the button of being able to complete the workout needs to be shown
  @Input() canCompleteWorkout: boolean = false;

  @Output() completeWorkout: EventEmitter<Workout> = new EventEmitter();
  
  constructor(
    private readonly programService: FitnessProgramService
  ) { }

  // Add/ removes the clicked workout from the currentlyEditedProgramWorkoutIndexes
  public EditWorkoutListOfProgram(index: number): void {
    this.programService.EditCurrentlyEditedProgramWorkoutIndexes(index);
  }

  // Gets whether the workout is in the currentlyEditedProgramWorkoutIndexes
  public GetIfWorkoutIdIsInProgram(index: number): boolean {
    return this.programService.currentlyEditedProgramWorkoutIndexes.includes(index);
  }

  // Send out the complete workout call
  public CompleteWorkout(workout: Workout) {
    this.completeWorkout.emit(workout);
  }
}
