import { Component, OnInit } from '@angular/core';
import { ExercisesService } from 'src/app/services/exercises.service';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';

@Component({
  selector: 'app-user-exercises-history',
  templateUrl: './user-exercises-history.component.html',
  styleUrls: ['./user-exercises-history.component.scss']
})
export class UserExercisesHistoryComponent implements OnInit {
  get exercises() { return this.exercisesService.userExercises; }
  get loading() { return this.exercisesService.loading; }
  get error() { return this.exercisesService.error; }
  constructor(private exercisesService:ExercisesService, private profileService: ProfileManagerService) { }

  ngOnInit(): void {
    if (this.profileService.currentProfile)
    {
      this.exercisesService.findExercisesByUserId(this.profileService.currentProfile?.userId);
    }
  }

}
