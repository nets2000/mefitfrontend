import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserExercisesHistoryComponent } from './user-exercises-history.component';

describe('UserExercisesHistoryComponent', () => {
  let component: UserExercisesHistoryComponent;
  let fixture: ComponentFixture<UserExercisesHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserExercisesHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserExercisesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
