import { Component } from '@angular/core';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  private fullName_!: string;

  // Gets full name of keycloak
  get fullName():string {
    if (keycloak.tokenParsed && keycloak.tokenParsed.name)
    {
    this.fullName_ = keycloak.tokenParsed.name;
    }
    return this.fullName_;
  }

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  // Checks whether the user has an profile in the active session
  get registered() : boolean {
    return this.profileManager.GetIfProfileExists();
  }

  // Checks whether the user is an contributor
  get isContributor(): boolean {
    if (!keycloak.authenticated) {
      return false;
    }
    return Boolean(keycloak.hasRealmRole('Contributor'));
  }

  // Checks whether the user is an admin
  get isAdmin(): boolean {
    if (!keycloak.authenticated) {
      return false;
    }
    return Boolean(keycloak.hasRealmRole('Administrator'));
  }

  constructor(
    private readonly profileManager: ProfileManagerService
  ) {  }

  // Logs out the user
  onLogoutClick(): void {
    keycloak.logout();
    this.profileManager.SetProfileToUndefined();
  }
}
