import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { RegisterExercise, Results } from 'src/app/models/register-exercise.model';
import { RegisterformComponent } from '../registerform/registerform.component';

@Component({
  selector: 'app-register-exercise',
  templateUrl: './register-exercise.component.html',
  styleUrls: ['./register-exercise.component.scss']
})
export class RegisterExerciseComponent implements OnInit {

  @ViewChild('answerInput') answerInput!: ElementRef;

  @Output() addRegisterExercise: EventEmitter<RegisterExerciseComponent> = new EventEmitter();
  
  // The register exercise that needs to be shown
  @Input() registerExercise!: RegisterExercise;

  private _showDescription: boolean = false;
  private _answers: string[] = [];

  constructor() { }

  get showDescription(): boolean {
    return this._showDescription;
  }

  get answers(): string[] {
    return this._answers;
  }

  get amountOfPointsForExercise(): number {
    return this._answers.indexOf(this.answerInput.nativeElement.value);
  }

  // Adds the answers to the list and emits a call with this object in it
  ngOnInit(): void {
    this.addRegisterExercise.emit(this);

    this._answers.push(this.registerExercise.Results.Poor);
    this._answers.push(this.registerExercise.Results.Fair);
    this._answers.push(this.registerExercise.Results.Good);
    this._answers.push(this.registerExercise.Results.Excellent);
  }

  // Shows/ hides the explanation of the register exercise
  public ShowDescription(): void {
    this._showDescription = !this._showDescription;
  }
}
