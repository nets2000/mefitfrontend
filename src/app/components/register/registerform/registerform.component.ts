import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Countries } from 'src/app/enums/countries.enum';
import { DisabilityType } from 'src/app/enums/disabilities.enum';
import { MedicalConditions } from 'src/app/enums/medical-conditions.enum';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from "../../../../environments/environment";
import keycloak from 'src/keycloak';
import { User, UserCreate } from 'src/app/models/user.model';
import { Address, AddressCreate } from 'src/app/models/address.model';
import { ProfileCreate } from 'src/app/models/profile.model';
import exerciseAssessmentJson from '../../../../assets/exercise-assesment-info.json';
import { RegisterExercise } from 'src/app/models/register-exercise.model';
import { RegisterExerciseComponent } from '../register-exercise/register-exercise.component';
import { FitnessLevel } from 'src/app/enums/fitness-level.enum';
import { ProfilePic, ProfilePicCreate } from 'src/app/models/profile-pic.model';
import { ProfileFormComponent } from '../../profile/profile-form/profile-form.component';

const { apiUrl } = environment;

@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.scss']
})
export class RegisterformComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter();

  private _profileForm: ProfileFormComponent | undefined = undefined;

  private _creatingNewProfile: boolean = false;

  private _allRegisterExerciseComponents: RegisterExerciseComponent[] = [];

  constructor(
    private readonly http: HttpClient
  ) { }

  get creatingNewProfile(): boolean {
    return this._creatingNewProfile;
  }

  get allRegisterExercises(): RegisterExercise[] {
    return exerciseAssessmentJson.Exercises;
  }

  get registerForm(): RegisterformComponent {
    return this;
  }

  get allRegisterExerciseComponents(): RegisterExerciseComponent[] {
    return this._allRegisterExerciseComponents;
  }

  get validForm(): boolean {
    return this._profileForm!.GetIfProfileFormIsValid();
  }

  // Send user out of login screen if the user already has a registered account
  ngOnInit(): void {
    this.http.get<any>(`${apiUrl}/users/${keycloak.tokenParsed?.sub}/profile`)
      .subscribe({
        next: (profile) => {
          // Log the user in if user already has an account
          this.login.emit();
        }
      })
  }

  // Convert the image url to a base 64 string
  private ConvertImageUrlToBase64(imageURL: string): string {
    var base64Index = imageURL.indexOf(';base64,') + ';base64,'.length;
    return imageURL.substring(base64Index);
  }

  // Start registering the new user
  public RegisterUser(): void {
    this._creatingNewProfile = true;

    // Check whether the user exists in the database
    this.http.get<any>(`${apiUrl}/users/${keycloak.tokenParsed?.sub}/userid`)
      .subscribe({
        next: (user) => {
          // If user does exist, go to check if address exists
          let currentUser: User = user;
          this.CheckForAddress(currentUser.id);
        },
        error: (error: HttpErrorResponse) => {
          // If not add profile picture to database
          if (error.status == 404) {
            this.AddProfilePictureToDatabase();
          } else {
            console.error(error.message);
          }
        }
      })
  }

  // Add new profile picture to database if user added one else go straight to creating a new user with a non-existent profile pic id
  private AddProfilePictureToDatabase() {
    if (this._profileForm!.imgURL === this._profileForm!.baseImage) {
      this.CreateNewUser(null);
    } else {
      let newProfilePic: ProfilePicCreate = {
        name: this._profileForm!.selectedFile!.name,
        type: this._profileForm!.selectedFile!.type,
        pic: this.ConvertImageUrlToBase64(this._profileForm!.imgURL)
      }

      this.http.post<ProfilePic>(`${apiUrl}/profilepics`, newProfilePic)
        .subscribe({
          next: (profilePic: ProfilePic) => {
            this.CreateNewUser(profilePic.id);
          },
          error: (error: HttpErrorResponse) => {
            console.log(error.message)
          }
        })
    }
  }

  // Create create user object and add new user to the database with all the filled in information and the information of keycloak
  private CreateNewUser(profilePictureId: number | null): void {
    let wantsToBeContributor: string = this._profileForm!.profileForm.get('wantsToBeContributor')!.value!;
    if (wantsToBeContributor == "") {
      wantsToBeContributor = String(false);
    }

    let newUser: UserCreate = {
      userId: keycloak.tokenParsed!.sub!,
      password: 'String',
      firstName: keycloak.tokenParsed!.given_name!,
      lastName: keycloak.tokenParsed!.family_name!,
      wantsToBeContributor: String(wantsToBeContributor),
      isContributor: String(false),
      isAdmin: String(false),
      profilePicId: profilePictureId
    }

    this.http.post<any>(`${apiUrl}/users`, newUser)
      .subscribe({
        next: (user) => {
          let currentUser: User = user;
          this.CheckForAddress(currentUser.id);
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Create create address object, then check whether the given address exists nad perform actions based on that
  private CheckForAddress(currentUserId: number): void {
    let country: string = this._profileForm!.profileForm.get('country')!.value!;
    if (country == "") {
      country = Object.values(Countries)[0];
    }

    let newAddress: AddressCreate = {
      addressLine1: this._profileForm!.profileForm.get('addressLine1')!.value!,
      addressLine2: this._profileForm!.profileForm.get('addressLine2')!.value!,
      addressLine3: this._profileForm!.profileForm.get('addressLine3')!.value!,
      country: country,
      city: this._profileForm!.profileForm.get('city')!.value!,
      postalCode: this._profileForm!.profileForm.get('postalCode')!.value!,
      profileIds: [],
    }

    this.http.get<any>(`${apiUrl}/addresses/${newAddress.postalCode}/adressbydata?city=${newAddress.city}&street=${newAddress.addressLine1}&houseNumber=${newAddress.addressLine2}`)
      .subscribe({
        next: (address) => {
          // Create a new profile with the address found
          let currentAddress: Address = address;
          this.CreateNewProfile(currentUserId, currentAddress.id);
        },
        error: (error: HttpErrorResponse) => {
          // If no address is found, create a new address with the information of the create address object
          if (error.status == 404) {
            this.CreateNewAddress(currentUserId, newAddress);
          } else {
            console.error(error.message);
          }
        }
      })
  }

  // Create a new address in the database and use its information for the new profile
  private CreateNewAddress(currentUserId: number, newAddress: AddressCreate): void {
    this.http.post<any>(`${apiUrl}/addresses`, newAddress)
      .subscribe({
        next: (address) => {
          let currentAddress: Address = address;
          this.CreateNewProfile(currentUserId, currentAddress.id);
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // Create a create profile object and use it to create a new profile
  private CreateNewProfile(currentUser: number, currentAddressId: number): void {
    let disabilities: string = this.GetStringValueFromPossibilities(this._profileForm!.profileForm.get('disabilities')!.value!, this._profileForm!.disabilityTypes[0],
      this._profileForm!.profileForm.get('otherDisabilities')!.value!);

    let medicalConditions: string = this.GetStringValueFromPossibilities(this._profileForm!.profileForm.get('medicalConditions')!.value!, this._profileForm!.medicalConditions[0],
      this._profileForm!.profileForm.get('otherMedicalConditions')!.value!);

    // Based on the input the user gave evaluate how fit they are a fitness level is selected
    // Each higher option selected from the fitness evaluation counts as one higher point with a minimum of 0 and maximum of 3
    // A percentage is calculated for the max score and each score level has a percentage assigned to it
    let fitnessEvaluation: number = 0;
    let maxFitnessEvaluation: number = 0;
    for (let i = 0; i < this._allRegisterExerciseComponents.length; i++) {
      fitnessEvaluation += this._allRegisterExerciseComponents[i].amountOfPointsForExercise;
      maxFitnessEvaluation += 3;
    }
    let fitnessLevelPercentage: number = fitnessEvaluation / maxFitnessEvaluation * 100;
    let fitnessLevel: number = FitnessLevel.Excellent;
    if (fitnessLevelPercentage < FitnessLevel.Fair) {
      fitnessLevel = FitnessLevel.Poor;
    } else if (fitnessLevelPercentage < FitnessLevel.Good) {
      fitnessLevel = FitnessLevel.Fair;
    } else if (fitnessLevelPercentage < FitnessLevel.Excellent) {
      fitnessLevel = FitnessLevel.Good;
    }

    let newProfile: ProfileCreate = {
      height: Number.parseFloat(this._profileForm!.profileForm.get('height')!.value!.replace(',', '.')),
      weight: Number.parseFloat(this._profileForm!.profileForm.get('weight')!.value!.replace(',', '.')),
      disabilities: disabilities,
      medicalConditions: medicalConditions,
      fitnessEvaluation: Object.keys(FitnessLevel)[Object.values(FitnessLevel).indexOf(fitnessLevel)].toString(),
      userId: currentUser,
      addressId: currentAddressId
    }

    this.http.post<any>(`${apiUrl}/profiles`, newProfile)
      .subscribe({
        next: (profile) => {
          // Log the user in once an account is created
          this.login.emit()
        },
        error: (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      })
  }

  // If no other option is selected from the dropdown the string is empty and thus the option from the dropdown is the first option
  // If the option selected is the Other... option then get the input from the input field
  private GetStringValueFromPossibilities(formField: string, firstOptionFromDropdown: string, otherInputField: string): string {
    let dropdownOption: string = formField;
    if (dropdownOption == "") {
      dropdownOption = firstOptionFromDropdown;
    } else if (dropdownOption == DisabilityType.Other) {
      dropdownOption = otherInputField;
    }

    return dropdownOption;
  }

  // Get the profile form
  public GetProfileForm(profileForm: ProfileFormComponent) {
    this._profileForm = profileForm;
  }

  public AddRegisterExerciseComponent(registerExerciseComponent: RegisterExerciseComponent) {
    this._allRegisterExerciseComponents.push(registerExerciseComponent);
  }
}
