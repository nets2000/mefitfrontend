import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Workout } from 'src/app/models/workout.model';
import { MuscleGroup } from 'src/app/models/muscleGroup.model';
import { GoalService } from 'src/app/services/goals.service';
import { WorkoutsService } from 'src/app/services/workouts.service';
import { FitnessProgram } from 'src/app/models/fitness-program.model';
import { FitnessProgramService } from 'src/app/services/fitness-program.service';
import { Exercise } from 'src/app/models/exercise.model';
import { ExercisesService } from 'src/app/services/exercises.service';
import { MuscleGroupService } from 'src/app/services/muscle-group.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';
import { FitnessLevel } from 'src/app/enums/fitness-level.enum';
import { ProgramCategory } from 'src/app/enums/programCategory.enum';
import { WorkoutDetailService } from 'src/app/services/workout-detail.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-goal-create-form',
  templateUrl: './goal-create-form.component.html',
  styleUrls: ['./goal-create-form.component.scss']
})
export class GoalCreateFormComponent implements OnInit {
  @ViewChild('paginator') paginator?: MatPaginator
  private todaysDate_!: string;
  private endOfWeek_!: string;
  private goalSuccess_: boolean = false;
  private custom_: boolean = false;
  private disabledWorkouts_: boolean = false;
  private _pageSize: number = 10;
  private _pageSizeOptions: number[] = [10, 25, 100];
  exerciseIds: number[] = []
  get noValues():boolean { return (!this.custom && this.workoutIds.length == 0) || (this.custom && this.exerciseIds.length == 0);}
  get doableCategories() { return this._doableCategories; }
  get workouts(): Workout[] { return this.workoutService.workouts; }
  get exercises(): Exercise[] { return this.exercisesService.exercises; }
  get fitnessPrograms(): FitnessProgram[] { return this.fitnessProgramService.allFitnessPrograms; }
  get loading(): boolean { return this.goalService.loading; }
  get todaysDate(): string { return this.todaysDate_; }
  get endOfWeek(): string { return this.endOfWeek_; }
  get goalSuccess(): boolean { return this.goalSuccess_; }
  get custom(): boolean { return this.custom_; }
  get disabledWorkouts(): boolean { return this.disabledWorkouts_; }
  get selectedExercises(): Exercise[] {
    return this.exercisesService.selectedExercises;
  }
  get workoutExercises() {
    return this.workoutDetailService.workoutExercises;
  }
  get limitExercises(): Exercise[] {
    return this.exercisesService.limitExercises;
  }
  get muscleGroups(): MuscleGroup[] {
    return this.muscleGroupService.muscleGroups;
  }

  get error(): HttpErrorResponse | undefined {
    return this.goalService.error;
  }

  get pageSizeOptions() {
    return this._pageSizeOptions;
  }

  workoutIds: number[] = []
  selectedMuscleGroup: number = 0;
  difficultyWarning: boolean = false;
  _doableCategories: string[] = [];
  
  constructor(
    private goalService: GoalService,
    private workoutService: WorkoutsService,
    private fitnessProgramService: FitnessProgramService,
    private exercisesService: ExercisesService,
    private muscleGroupService: MuscleGroupService,
    private readonly profileManager: ProfileManagerService ,
    private workoutDetailService:WorkoutDetailService
  ) { }

  ngOnInit(): void {
    this.setDoableCategories();
    let date = new Date();
    // Limits the range of options for the endDate to one week (current day until sunday)
    this.todaysDate_ = date.toISOString().split('Z')[0];
    this.endOfWeek_ = new Date(date.getFullYear(), date.getMonth(), date.getDate() + (8 - date.getDay())).toISOString().split('Z')[0]
  }

  // Form submit
  public goalSetSubmit(goalSetForm: NgForm) {
    const { endDate, fitnessProgram } = goalSetForm.value;
    this.goalService.createGoal(endDate, this.workoutIds, this.exerciseIds, fitnessProgram).add(() => {
      if (this.goalService.createdGoal) 
      {
        this.goalSuccess_ = true;
        this.exerciseIds = [];
        this.workoutIds = [];
        goalSetForm.resetForm();
      }
      else 
      {
        this.goalSuccess_ = false;
      }
    }
    );
  }

  public setDoableCategories(): void {
      const fitnessLevel = this.profileManager.currentProfile?.fitnessLevel;
      if (fitnessLevel != null)
      {
        if (fitnessLevel >= FitnessLevel.Poor)
        {
          this._doableCategories.push(ProgramCategory.Relax, ProgramCategory.Starter);
        }
        if (fitnessLevel >= FitnessLevel.Fair)
        {
          this._doableCategories.push(ProgramCategory.Sportive);
        }
        if (fitnessLevel >= FitnessLevel.Good)
        {
          this._doableCategories.push(ProgramCategory.Intense)
        }
        if (fitnessLevel >= FitnessLevel.Excellent)
        {
          this._doableCategories.push(ProgramCategory.Insane, ProgramCategory.Tough);
        }
      }
  }

  // Listens for click on checkbox of workouts
  public workoutCheckboxChange(workout: Workout) {
    if (this.workoutIds.indexOf(workout.id) == -1) {
      this.workoutIds.push(workout.id);
    }
    else this.workoutIds.splice(this.workoutIds.indexOf(workout.id), 1)
  }

  // Listens for click on checkbox of exercise
  changeExercises(exerciseId: any) {
    if (this.exerciseIds.indexOf(exerciseId) == -1) {
      this.exerciseIds.push(exerciseId)
    }
    else {
      this.exerciseIds.splice(this.exerciseIds.indexOf(exerciseId), 1)
    }
  }

  // Turns on/off custom goal creation
  public changeCustom() {
    this.workoutIds = []
    this.exerciseIds = []
    this.custom_ = !this.custom_;
  }

  public isChecked(id: number) {
    return this.workoutIds.indexOf(id) != -1;
  }

  public changeWorkouts(event: any) {
    // Change the workouts to be disabled and only of the current programm
    let selectedOption = event.target.value
    if (selectedOption == -1) {
      this.disabledWorkouts_ = false;
      this.changeCustom()
      this.difficultyWarning = false;
    }
    else {
      this.disabledWorkouts_ = true;
      let workoutsInProgram: number[] = [];
      this.fitnessPrograms.forEach(program => {
        if (program.id == selectedOption)
        {
          workoutsInProgram = program.workoutIds;
          if (this._doableCategories.indexOf(program.category) == -1)
            this.difficultyWarning = true;
          else {
            this.difficultyWarning = false; 
          }
        }
      })
      if (workoutsInProgram.length != 0) {
        this.workoutIds = workoutsInProgram;
      }
    }
  }

  changeMuscleGroup(e: any) {
    this.selectedMuscleGroup = e.target.value;
    if (this.selectedMuscleGroup == 0) {
      this.exercisesService.selectedExercises = this.exercisesService.exercises;
    } else {
      this.exercisesService.findExercisesWithMuscleGroup(this.selectedMuscleGroup);
    }

    this.paginator?.firstPage();
    this.SetPageContent(0);
  }

  OnPageChange(event: PageEvent) {
    this.exercisesService.limitExercises = [];
    const startIndex = event.pageIndex * this._pageSize;

    this.SetPageContent(startIndex);
  }

  private SetPageContent(startIndex: number) {
    let endIndex = startIndex + this._pageSize;
    if (endIndex > this.selectedExercises.length) {
      endIndex = this.selectedExercises.length;
    }
    this.exercisesService.limitExercises = this.selectedExercises.slice(startIndex, endIndex);
  }

  public ChangePageSizeEvent(e: any) {
    this._pageSize = Number.parseInt(e.target.value);
    this.ChangePageSize(this._pageSize);
  }

  private ChangePageSize(pageSize: number) {
    this.paginator?._changePageSize(pageSize);
    this.paginator?.firstPage();
  }
}
