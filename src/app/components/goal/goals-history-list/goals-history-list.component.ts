import { Component, Input, OnInit } from '@angular/core';
import { Goal } from 'src/app/models/goals.model';
@Component({
  selector: 'app-goals-history-list',
  templateUrl: './goals-history-list.component.html',
  styleUrls: ['./goals-history-list.component.scss']
})
export class GoalsHistoryListComponent implements OnInit {

  @Input() pastGoals!:Goal[]
  constructor() {
   }

  ngOnInit(): void {
  }

}
