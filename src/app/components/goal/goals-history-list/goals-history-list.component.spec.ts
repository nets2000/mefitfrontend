import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalsHistoryListComponent } from './goals-history-list.component';

describe('GoalsHistoryListComponent', () => {
  let component: GoalsHistoryListComponent;
  let fixture: ComponentFixture<GoalsHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalsHistoryListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoalsHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
