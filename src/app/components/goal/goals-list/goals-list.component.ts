import { Component, Input, OnInit } from '@angular/core';
import { GoalDetailService } from 'src/app/services/goals-detail.service';
import { Goal } from 'src/app/models/goals.model';
@Component({
  selector: 'app-goals-list',
  templateUrl: './goals-list.component.html',
  styleUrls: ['./goals-list.component.scss']
})
export class GoalsListComponent implements OnInit {
@Input() goals!:Goal[]
  constructor() { }

  ngOnInit(): void {
  }
}
