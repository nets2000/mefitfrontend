import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalsHistoryListItemComponent } from './goals-history-list-item.component';

describe('GoalsHistoryListItemComponent', () => {
  let component: GoalsHistoryListItemComponent;
  let fixture: ComponentFixture<GoalsHistoryListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalsHistoryListItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoalsHistoryListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
