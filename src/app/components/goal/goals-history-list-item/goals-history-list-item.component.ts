import { Component, Input, OnInit } from '@angular/core';
import { Goal } from 'src/app/models/goals.model';

@Component({
  selector: 'app-goals-history-list-item',
  templateUrl: './goals-history-list-item.component.html',
  styleUrls: ['./goals-history-list-item.component.scss']
})
export class GoalsHistoryListItemComponent implements OnInit {
  @Input() goal!:Goal;
  constructor() { 
  }
  
  ngOnInit(): void {
  }

}
