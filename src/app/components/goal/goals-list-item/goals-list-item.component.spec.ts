import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalsListItemComponent } from './goals-list-item.component';

describe('GoalsListItemComponent', () => {
  let component: GoalsListItemComponent;
  let fixture: ComponentFixture<GoalsListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalsListItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoalsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
