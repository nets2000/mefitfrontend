import { Component, Input, OnInit } from '@angular/core';

import { ChartType, ChartData } from 'chart.js';

import { Exercise } from 'src/app/models/exercise.model';
import { Goal } from 'src/app/models/goals.model';
import { Workout } from 'src/app/models/workout.model';
import { GoalDetailService } from 'src/app/services/goals-detail.service';
import { GoalService } from 'src/app/services/goals.service';

@Component({
  selector: 'app-goals-list-item',
  templateUrl: './goals-list-item.component.html',
  styleUrls: ['./goals-list-item.component.scss']
})
export class GoalsListItemComponent implements OnInit {
  @Input() selectedGoal!: Goal;
  loading_: boolean = false;
  private percentage_: number = 0;
  get workouts(): Workout[] { return this.goalService.goalWorkouts; }
  
  private _dataForChart: number[] = [];
  completeChart: ChartData | undefined = undefined;

  doughnutChartType: ChartType = 'doughnut';

  public chartOptions: any = {
    responsive: true,
    animations: false,
    cutout: '85%',
    plugins: {
      tooltip: { enabled: false },
    },
    hover: { mode: null },
  }

  get percentage() { return this.percentage_; }
  get loading() { return this.goalDetailService.loading || this.loading_; }
  get exercises(): Exercise[] { return this.goalService.goalExercises; }

  constructor(
    private goalDetailService: GoalDetailService,
    private goalService: GoalService
  ) { }

  ngOnInit(): void {
    this.percentage_ = 0;
    this.loading_ = true;
    this.setPercentage();
  }

  /*
    Function to set the completed percentage of workouts and exercises combined
    Retrieves all workouts and exercises from the services. 
  */
  setPercentage() {
    this.goalDetailService.findById(this.selectedGoal.id);
    this.goalService.findWorkoutsByGoalId(this.selectedGoal.id)
      .add(() => {
        this.goalService.findExercisesByGoalId(this.selectedGoal.id).add(() => {
          this.percentage_ = parseInt(this.goalService.completedPercentage(this.workouts, this.exercises));
          
          this._dataForChart = [this.percentage_, 100 - this.percentage_];
          this.completeChart = {
            datasets: [{
              data: this._dataForChart,
              borderWidth: 1,
              backgroundColor: [
                'rgba(0, 71, 232, 1)',
                'rgba(182, 187, 198, 1)'
              ],
              borderColor: [
                'rgba(0, 71, 232, 1)',
                'rgba(182, 187, 198, 1)'
              ]
            }],
          };

          if (this.percentage == 100) {
            this.goalDetailService.goalAchieved(this.selectedGoal.id).add(() => {
            })
            this.selectedGoal.achieved = true;
          }
        })
        this.loading_ = false;
      })
  }
}
