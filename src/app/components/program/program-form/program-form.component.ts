import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProgramCategory } from 'src/app/enums/programCategory.enum';
import { CreateFitnessProgram, FitnessProgram, UpdateFitnessProgram } from 'src/app/models/fitness-program.model';
import { Workout } from 'src/app/models/workout.model';
import { FitnessProgramService } from 'src/app/services/fitness-program.service';
import { WorkoutsService } from 'src/app/services/workouts.service';
import keycloak from 'src/keycloak';
import { environment } from "../../../../environments/environment";

const { apiUrl } = environment;

@Component({
  selector: 'app-program-form',
  templateUrl: './program-form.component.html',
  styleUrls: ['./program-form.component.scss']
})
export class ProgramFormComponent {

  private _currentProgram: FitnessProgram | undefined = undefined;
  private _creatingProgram: boolean = false;
  private _showCreateNewProgram: boolean = false;
  private _error?:HttpErrorResponse;
  private _success:boolean = false;

  public programForm = this.builder.group({
    name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    category: ['']
  });

  private _programTypes: string[] = [];

  get allPrograms(): FitnessProgram[] {
    return this.programsService.allFitnessPrograms;
  }

  get programTypes(): string[] {
    return this._programTypes;
  }

  get showCreateNewProgram(): boolean {
    return this._showCreateNewProgram;
  }

  get allWorkouts(): Workout[] {
    return this.workoutsService.workouts;
  }

  get editingProgram(): boolean {
    return Boolean(this._currentProgram);
  }
  get error():HttpErrorResponse | undefined {
    return this._error;
  }
  get success():boolean { 
    return this._success;
  }
  get loading():boolean {
    return this.programsService.loading;
  }
  get currentProgram() {
    return this._currentProgram;
  }
  constructor(
    private readonly http: HttpClient,
    private readonly programsService: FitnessProgramService,
    private readonly builder: FormBuilder,
    private readonly workoutsService: WorkoutsService
  ) { }

  // Get all program types
  private FillProgramTypes(): void {
    this._programTypes = [];

    // If editing a program, add program type of edited program first so it shows first on dropdown
    if (this._currentProgram) {
      this.programTypes.push(this._currentProgram.category);
    }

    const programTypes = Object.values(ProgramCategory);

    programTypes.forEach((value) => {
      if (!this._programTypes.includes(value)) {
        this._programTypes.push(value);
      }
    });
  }

  // Start/ stop showing the page of creating a new program
  public ShowCreateNewProgram(program: FitnessProgram | undefined): void {
    this._showCreateNewProgram = !this._showCreateNewProgram;

    // Set the data to the currently edited program if there is one, else reset all the data
    if (this._showCreateNewProgram) {
      this._currentProgram = program;
      this.FillProgramTypes();

      if (program) {
        this.programsService.currentlyEditedProgramWorkoutIndexes = program.workoutIds;
        this.programForm.get('name')!.setValue(program.name);
      } else {
        this.programsService.currentlyEditedProgramWorkoutIndexes = [];
        this.programForm.get('name')!.setValue("");
      }

      this.programForm.get('name')?.markAsUntouched();
      this.programForm.get('category')?.setValue("");
    }
  }

  // Create a new program in the database
  public CreateNewProgram(): void {
    this._creatingProgram = false;

    // If data in a dropdown in not changed its an empty string and thus can be set to the first option of the dropdown
    let category: string = this.programForm.get('category')!.value!;
    console.log(category);
    if (category == "") {
      category = this._programTypes[0];
    }

    // Based on whether a program is set, update the program or create a new one
    if (this._currentProgram) {
      let updatedProgram: UpdateFitnessProgram = {
        id: this._currentProgram!.id,
        name: this.programForm.get('name')!.value!,
        category: ProgramCategory[category as keyof typeof ProgramCategory]
      }

      // Update program based on given data
      this.http.put<any>(`${apiUrl}/fitnessprogram?id=${this._currentProgram!.id}`, updatedProgram)
      .subscribe({
        next: (program) => {
          let currentProgram: FitnessProgram = program;
          this.AddWorkoutsToProgram(currentProgram.id);
          this._success = true;
        },
        error: (error: HttpErrorResponse) => {
          this._creatingProgram = false;
          this._error = error;
        }
      })
    } else {
      let newProgram: CreateFitnessProgram = {
        name: this.programForm.get('name')!.value!,
        category: ProgramCategory[category as keyof typeof ProgramCategory]
      }

      // Create new program based on given data
      this.http.post<any>(`${apiUrl}/fitnessprogram`, newProgram)
      .subscribe({
        next: (program) => {
          let currentProgram: FitnessProgram = program;
          this.AddWorkoutsToProgram(currentProgram.id);
          this._success = true;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error;
          this._creatingProgram = false;
        }
      })
    }
  }

  // Set the selected workouts to the program
  public AddWorkoutsToProgram(programId: number): void {
    this.http.put<any>(`${apiUrl}/fitnessprogram/${programId}/workouts`, this.programsService.currentlyEditedProgramWorkoutIndexes)
      .subscribe({
        next: () => {
          // Update program list
          this.programsService.GetAllPrograms(this);
          this._success = true;
        },
        error: (error: HttpErrorResponse) => {
          this._creatingProgram = false;
          this._error = error;
        }
      })
  }

  // Switch page back to show all programs list
  public FinishAddingWorkoutsToProgram() {
    this._creatingProgram = false;
    this._showCreateNewProgram = false;
  }

  // Get the names of all the currently added workouts to the program
  public GetNamesOfAllCurrentlyAddedWorkouts(): string {
    let amountOfIndexes: number = this.programsService.currentlyEditedProgramWorkoutIndexes.length;
    if (amountOfIndexes > 0) {
      let currentlySelectedWorkouts: string = '';
      for (let i = 0; i < amountOfIndexes; i++) {
        currentlySelectedWorkouts += this.workoutsService.workouts.find(workout => workout.id == this.programsService.currentlyEditedProgramWorkoutIndexes[i])!.name;

        if (i < amountOfIndexes - 1) {
          currentlySelectedWorkouts += ", ";
        }
      }

      return currentlySelectedWorkouts;
    } else {
      // If there are no workouts added, display this instead
      return "You need to add a workout to the program";
    }
  }

  // Check whether the add program button can be pressed
  public CheckIfCanAddProgram(): boolean {
    if (this.programsService.currentlyEditedProgramWorkoutIndexes.length > 0 && this.programForm.valid && !this._creatingProgram) {
      return true;
    }

    return false;
  }
}
