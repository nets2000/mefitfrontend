import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FitnessProgramService } from 'src/app/services/fitness-program.service';
import { FitnessProgram } from 'src/app/models/fitness-program.model';
import { ProgramCategory } from 'src/app/enums/programCategory.enum';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: ['./program-list.component.scss']
})
export class ProgramListComponent implements OnInit {

  @Output() editProgram: EventEmitter<FitnessProgram> = new EventEmitter();

  // Makes it able for the program detail button to show the edit program button
  @Input() ableToEditProgram: boolean = false;
  // All the fitness programs that it needs to show
  @Input() currentFitnessPrograms!: FitnessProgram[];

  @ViewChild('paginator') paginator?: MatPaginator;
  @ViewChild('programTypes') programTypes!: ElementRef;

  private _pageSize: number = 0;
  private _pageSizeOptions: number[] = [3, 5, 10];

  private _allProgramTypes: string = "All";

  private _loading: boolean = true;

  private _allPossibleTypesOfPrograms: string[] = [];

  private _currentlySelectedFitnessPrograms: FitnessProgram[] = [];
  private _currentlyShowingFitnessPrograms: FitnessProgram[] = [];

  constructor() { }

  get currentlySelectedFitnessPrograms(): FitnessProgram[] {
    return this._currentlySelectedFitnessPrograms;
  }

  get currentlyShowingFitnessPrograms(): FitnessProgram[] {
    return this._currentlyShowingFitnessPrograms;
  }

  get allPossibleTypesOfPrograms(): string[] {
    return this._allPossibleTypesOfPrograms;
  }

  get loading(): boolean {
    return this._loading;
  }

  get pageSizeOptions(): number[] {
    return this._pageSizeOptions;
  }

  get firstPageSizeOptions(): number {
    return this._pageSizeOptions[0];
  }
  // get error():HttpErrorResponse | undefined {
  //   // return this.service
  // }
  // Gets all current fitness programs when available
  ngOnInit(): void {
    if (this.currentFitnessPrograms.length > 0) {
      this.EndLoading();
    } else {
      setTimeout(() => {
        this.ngOnInit();
      }, 100);
    }
  }

  // Initialize required data 
  public EndLoading(): void {
    this._loading = false;

    // Set data for the pages
    this._pageSize = this._pageSizeOptions[0];
    this._currentlySelectedFitnessPrograms = this.currentFitnessPrograms;
    this._currentlyShowingFitnessPrograms = this.currentFitnessPrograms.slice(0, this._pageSize);

    // Set data for program category selection dropdown
    this._allPossibleTypesOfPrograms.push(this._allProgramTypes);
    const programTypes = Object.values(ProgramCategory);

    programTypes.forEach((value) => {
      if (this._currentlySelectedFitnessPrograms.find(programType => programType.category == value)) {
        this._allPossibleTypesOfPrograms.push(value);
      }
    });
  }

  // Set selected programs equal to all the programs that have the chosen program type, if type is this._allProgramTypes, then show all programs
  public OnProgramTypeSelect(): void {
    this._currentlySelectedFitnessPrograms = [];
    let chosenProgramType: string = this.programTypes.nativeElement.value;

    if (chosenProgramType != this._allProgramTypes) {
      for (let i = 0; i < this.currentFitnessPrograms.length; i++) {
        if (this.currentFitnessPrograms[i].category == chosenProgramType) {
          this._currentlySelectedFitnessPrograms.push(this.currentFitnessPrograms[i]);
        }
      }
    } else {
      this._currentlySelectedFitnessPrograms = this.currentFitnessPrograms;
    }

    // Reset pages to first page
    this.paginator?.firstPage();
    this.SetPageContent(0);
  }

  // The function that is called when a new page is shown
  OnPageChange(event: PageEvent) {
    this._currentlyShowingFitnessPrograms = [];
    const startIndex = event.pageIndex * this._pageSize;

    this.SetPageContent(startIndex);
  }

  // Sets the content of the page based on which programs are currently selected and on which page and how many to show
  private SetPageContent(startIndex: number) {
    let endIndex = startIndex + this._pageSize;
    if (endIndex > this._currentlySelectedFitnessPrograms.length) {
      endIndex = this._currentlySelectedFitnessPrograms.length;
    }

    this._currentlyShowingFitnessPrograms = this._currentlySelectedFitnessPrograms.slice(startIndex, endIndex);
  }

  // The event that is called when a different page size is selected
  public ChangePageSizeEvent(e: any) {
    this._pageSize = Number.parseInt(e.target.value);
    this.ChangePageSize(this._pageSize);
  }

  // Change the page size to the chosen amount
  private ChangePageSize(pageSize: number) {
    this.paginator?._changePageSize(pageSize);
    this.paginator?.firstPage();
  }

  // Sends a fitness program upwards
  public EditProgram(program: FitnessProgram): void {
    this.editProgram.emit(program);
  }
}
