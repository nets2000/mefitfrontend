import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FitnessProgram } from 'src/app/models/fitness-program.model';
import { ProgramListComponent } from '../program-list/program-list.component';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from "../../../../environments/environment";
import { FitnessProgramService } from 'src/app/services/fitness-program.service';
import { Workout } from 'src/app/models/workout.model';

const { apiUrl } = environment;

@Component({
  selector: 'app-program-details',
  templateUrl: './program-details.component.html',
  styleUrls: ['./program-details.component.scss']
})
export class ProgramDetailsComponent implements OnInit {

  @Output() editProgram: EventEmitter<FitnessProgram> = new EventEmitter();

  // Enable this if the edit program button needs to be shown
  @Input() ableToEditProgram: boolean = false;
  // The fitness program where to details need to be shown from
  @Input() program!: FitnessProgram;

  private _workoutsOfProgram: Workout[] = [];

  private _showWorkouts: boolean = false;

  get workoutsOfProgram(): Workout[] {
    return this._workoutsOfProgram;
  }

  get showWorkouts(): boolean {
    return this._showWorkouts;
  }
  get error(): HttpErrorResponse | undefined {
    return this.fitnessProgramService.error;
  }
  constructor(
    private readonly fitnessProgramService: FitnessProgramService
  ) { }

  // Get all workouts of program 
  ngOnInit(): void {
    this.fitnessProgramService.GetWorkoutsOfProgram(this.program!.id).forEach(workout => this._workoutsOfProgram = workout);
  }

  // Switch visible state of showing the workouts of the program
  public ShowWorkoutsOfProgram() {
    this._showWorkouts = !this._showWorkouts;
  }

  // Signal that the edit program button is pressed and send the program that needs to be edited with it
  public EditProgram(): void {
    this.editProgram.emit(this.program);
  }
}
