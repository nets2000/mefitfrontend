import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirect-timer',
  templateUrl: './redirect-timer.component.html',
  styleUrls: ['./redirect-timer.component.scss']
})
export class RedirectTimerComponent implements OnInit {
  @Input() time!:number;
  constructor(private router:Router) { }

  ngOnInit(): void {
      setInterval(() => {
        this.time--;
      }, 1000)
  }

}
