import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectTimerComponent } from './redirect-timer.component';

describe('RedirectTimerComponent', () => {
  let component: RedirectTimerComponent;
  let fixture: ComponentFixture<RedirectTimerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedirectTimerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedirectTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
