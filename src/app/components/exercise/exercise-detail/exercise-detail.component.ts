import {Component, Input, OnInit } from '@angular/core';
import {Exercise} from "src/app/models/exercise.model";
import {MuscleGroup} from "src/app/models/muscleGroup.model";
import { ExerciseListItemComponent } from 'src/app/components/exercise/exercise-list-item/exercise-list-item.component';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-exercise-detail',
  templateUrl: './exercise-detail.component.html',
  styleUrls: ['./exercise-detail.component.scss']
})
export class ExerciseDetailComponent implements OnInit {
  // Sanitized URL to combat XSS
  trustedLink? : SafeUrl;
  embedLink?:string;
  validLink:boolean = false;
  // The exercise that needs to be shown
  @Input() selectedExercise!: Exercise;
  // The exercise list component to help show the muscle groups
  @Input() exerciseListItem!: ExerciseListItemComponent;

  get muscleGroups(): MuscleGroup[] {
    return this.exerciseListItem.muscleGroups;
  }
  // Only allow youtube links
  validateLink() {
    if (this.selectedExercise.vidLink)
    {
      // Construct a safe youtube embed link if vidLink is a youtube URL 
      if (this.selectedExercise.vidLink.includes('https://youtu.be'))
      {
        if (this.embedLink == undefined) this.makeUrl(1)
        this.trustedLink = this.sanitizer.bypassSecurityTrustResourceUrl(`${this.embedLink}?enablejsapi=1&origin:http://localhost:4200`);
        return true;
      }
      else if (this.selectedExercise.vidLink.includes('youtube.com/watch')){
        if (this.embedLink == undefined) this.makeUrl(2)
        this.trustedLink = this.sanitizer.bypassSecurityTrustResourceUrl(`${this.embedLink}?enablejsapi=1&origin:http://localhost:4200`);
        return true;
      }
      return false;
    }
    return false;
  }
  makeUrl(type: number)
  {
    switch (type)
    {
      case 1:
        this.embedLink = `https://www.youtube-nocookie.com/embed/${this.selectedExercise.vidLink.split('/')[3]}`;
        break;
      case 2:
        this.embedLink = `https://www.youtube-nocookie.com/embed/${this.selectedExercise.vidLink.split('?')[1]}`;
        break;
    }
  }
  constructor(private sanitizer:DomSanitizer) {}
  ngOnInit(): void {
      this.validLink = this.validateLink();
  }
  
}
