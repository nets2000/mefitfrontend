import { Component, Input, OnInit } from '@angular/core';
import { Exercise } from "src/app/models/exercise.model";
import { MuscleGroup } from "src/app/models/muscleGroup.model";
import { MuscleGroupService } from 'src/app/services/muscle-group.service';

@Component({
  selector: 'app-exercise-list-item',
  templateUrl: './exercise-list-item.component.html',
  styleUrls: ['./exercise-list-item.component.scss']
})
export class ExerciseListItemComponent implements OnInit {

  // The exercise that needs to be shown
  @Input() exercise!: Exercise;
  private _muscleGroups: MuscleGroup[] = [];

  private _showExerciseDetails: boolean = false;

  get muscleGroups(): MuscleGroup[] {
    return this._muscleGroups;
  }

  get showExerciseDetails(): boolean {
    return this._showExerciseDetails;
  }

  constructor(
    private readonly muscleGroupService: MuscleGroupService
  ) { }

  // If there is no exercise don't try to find muscle groups, else search for muscle groups
  ngOnInit(): void {
    if (!this.exercise) { return; }
    this.FindMuscleGroupsOfExercise();
  }

  // Get the muscle groups of the exercise based on the given muscle group ids in the exercise once the muscle group service has all muscle groups
  private FindMuscleGroupsOfExercise(): void {
    if (this.muscleGroupService.muscleGroups.length != 0) {
      for (let i = 0; i < this.exercise.muscleGroupIds.length; i++) {
        this._muscleGroups.push(this.muscleGroupService.muscleGroups.find(musclegroup => musclegroup.id == this.exercise.muscleGroupIds[i])!);
      }
    } else {
      setTimeout(() => {
        this.FindMuscleGroupsOfExercise();
      }, 100);
    }
  }

  // Switches visibility of the exercise details
  public FlipShowExerciseDetails(): void {
    this._showExerciseDetails = !this._showExerciseDetails;
  }
}
