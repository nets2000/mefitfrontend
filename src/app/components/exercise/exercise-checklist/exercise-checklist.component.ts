import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Exercise } from 'src/app/models/exercise.model';
import { WorkoutDetailService } from 'src/app/services/workout-detail.service';

@Component({
  selector: 'app-exercise-checklist',
  templateUrl: './exercise-checklist.component.html',
  styleUrls: ['./exercise-checklist.component.scss']
})
export class ExerciseChecklistComponent {
  // A list of exercises that should be displayed
  @Input() exercises!:Exercise[];
  // A list of all exercises that should be checked
  @Input() checkedExercises: number[] = [];
  @Output() changedExercises: EventEmitter<number> = new EventEmitter<number>()

  get loading(): boolean { return this.workoutService.loading; }
  constructor(private workoutService:WorkoutDetailService) { }

  // When an exercise check button is clicked, call this function and send which exercise was clicked
  changeExercises(exercise:Exercise)
  {
    this.changedExercises.emit(exercise.id)
  }
}
