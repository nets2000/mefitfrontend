import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseChecklistComponent } from './exercise-checklist.component';

describe('ExerciseChecklistComponent', () => {
  let component: ExerciseChecklistComponent;
  let fixture: ComponentFixture<ExerciseChecklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseChecklistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
