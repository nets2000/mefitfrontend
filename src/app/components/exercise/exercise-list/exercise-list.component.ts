import {Component, Input, OnInit} from '@angular/core';
import {Exercise} from "src/app/models/exercise.model";

@Component({
  selector: 'app-exercise-list',
  templateUrl: './exercise-list.component.html',
  styleUrls: ['./exercise-list.component.scss'],
  exportAs: 'cdkAccordionItem'
})
export class ExerciseListComponent {

  // The list of exercises that need to be displayed
  @Input() exercises!: Exercise[];

  constructor() { }
}
