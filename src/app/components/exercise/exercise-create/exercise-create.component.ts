import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Exercise } from 'src/app/models/exercise.model';
import { ExercisesService } from 'src/app/services/exercises.service';
import { MuscleGroupService } from 'src/app/services/muscle-group.service';
import { ProfileManagerService } from 'src/app/services/profile-manager.service';

@Component({
  selector: 'app-exercise-create',
  templateUrl: './exercise-create.component.html',
  styleUrls: ['./exercise-create.component.scss']
})
export class ExerciseCreateComponent implements OnInit {
  @Input() editExercise?:Exercise;
  muscleGroupIds:number[] = [];
  private success_:boolean = false;
  private exerciseId!:number;
  private exercise_!:Exercise;
  editName:boolean = false;
  editDescription:boolean = false;
  editImage:boolean = false;
  editVidLink:boolean = false;
  get success():boolean { return this.success_; }
  get loading() { return this.muscleGroupsService.loading || this.exercisesService.loading; }
  get error() { return this.muscleGroupsService.error; }
  get muscleGroups() { return this.muscleGroupsService.muscleGroups;}
  get exercises() { return this.exercisesService.exercises; }
  get exercise() { return this.exercise_; }
  constructor(
    private exercisesService:ExercisesService,
    private muscleGroupsService:MuscleGroupService,
    private profileManagerService:ProfileManagerService,
    private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.muscleGroupsService.findAllMuscleGroups();
      if (this.editExercise != undefined)
      {
        this.exercise_ = this.editExercise;
        this.exercisesService.findMuscleGroupsFromExercise(this.exerciseId)
        .subscribe(
          {next: (muscleGroups) => {
            muscleGroups.forEach(muscleGroup => {
              this.muscleGroupIds.push(muscleGroup.id);
            });
          }}
        ); 
      }
    else {
      this.exercisesService.findAllExercises();
    }
  }
  editItem(itemName:string)
  {
    switch (itemName)
    {
      case "name":
        this.editName = !this.editName;
        break;
      case "description":
        this.editDescription = !this.editDescription;
        break;
      case "image":
        this.editImage = !this.editImage;
        break;
      case "video":
        this.editVidLink = !this.editVidLink;
        break;
      
    }
  }
  exerciseCreateSubmit(exerciseForm:NgForm)
  {
    const {exerciseName, exerciseDescription, exerciseImage, exerciseVidLink} = exerciseForm.value;
    if (this.exercise != null)
    {
      this.exercise.name = exerciseName;
      this.exercise.description = exerciseDescription;
      this.exercise.image = exerciseImage;
      this.exercise.vidLink = exerciseVidLink;
      this.exercise.muscleGroupIds = this.muscleGroupIds; 
      if (this.profileManagerService.currentProfile) 
        this.exercisesService.updateExercise(this.exercise).add(() => this.success_ = true);
    }
    else {
      if (this.profileManagerService.currentProfile)
      {
        this.exercisesService.createNewExercise({
          'name': exerciseName,
          'description':exerciseDescription,
          'image': exerciseImage,
          'vidLink': exerciseVidLink,
          'muscleGroupIds': this.muscleGroupIds,
          'reps': 10,
          'maxReps': 15,
          'minReps': 5,
          'totalSets': 20,
          'userId': this.profileManagerService.currentProfile?.userId})
          .add(() => this.success_ = true)
        }
      }
  }
  changeMuscleGroups(muscleGroupId:number)
  {
    if (!this.isChecked(muscleGroupId))
    {
      this.muscleGroupIds.push(muscleGroupId);
    }
    else {
      this.muscleGroupIds.splice(this.muscleGroupIds.indexOf(muscleGroupId), 1);
    }
  }
  isChecked(muscleGroupId:number)
  {
    return this.muscleGroupIds.indexOf(muscleGroupId) != -1;
  }

}
