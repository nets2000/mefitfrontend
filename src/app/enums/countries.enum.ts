// All existing types of countries in the program. Add one to the enum to create new country
export enum Countries {
    Netherlands = "Netherlands",
    Denmark = "Denmark",
    Sweden = "Sweden",
    Norway = "Norway",
    Finland = "Finland",
    Belgium = "Belgium",
    Germany = "Germany",
    GreatBritain = "Great Britain",
    France = "France"
}