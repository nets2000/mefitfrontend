// All existing types of disabilities. Add one to the enum to create new disability
export enum DisabilityType {
    None = "None",
    Disability1 = "Disability 1",
    Disability2 = "Disability 2",
    Disability3 = "Disability 3",
    Disability4 = "Disability 4",
    Disability5 = "Disability 5",
    Disability6 = "Disability 6",
    Other = "Other..."
}