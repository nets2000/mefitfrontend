// All existing types of programs. Add one to the enum to create new program type
export enum ProgramCategory
{
    Tough = "Tough",
    Sportive = "Sportive",
    Relax = "Relax",
    Intense = "Intense",
    Insane = "Insane",
    Starter = "Starter"
}