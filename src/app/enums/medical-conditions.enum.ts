// All existing types of medical conditions. Add one to the enum to create new medical condition
export enum MedicalConditions {
    None = "None",
    MedicalCondition1 = "Medical condition 1",
    MedicalCondition2 = "Medical condition 2",
    MedicalCondition3 = "Medical condition 3",
    MedicalCondition4 = "Medical condition 4",
    MedicalCondition5 = "Medical condition 5",
    MedicalCondition6 = "Medical condition 6",
    Other = "Other..."
}