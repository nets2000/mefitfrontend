// All existing types of workouts. Add one to the enum to create new workout type
export enum WorkoutTypes {
    UpperBody = "Upper body",
    LowerBody = "Lower body",
    FullBody = "Full body"
}