// The required numbers to reach certain fitness levels. Each number is a percentage.
export enum FitnessLevel {
    Excellent = 80,
    Good = 60,
    Fair = 40,
    Poor = 0,
}