import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { ContributorDashboardPage } from './pages/contributor-dashboard/contributor-dashboard.page';
import { LoginPage } from './pages/login/login.page';
import { GoalsDashboardPage } from './pages/goals-dashboard/goals-dashboard.page';
import { ProfilePage } from './pages/profile/profile.page';
import {GoalDetailPage} from "./pages/goal-detail/goal-detail.page";
import { GoalCreatePage } from './pages/goal-create/goal-create.page';
import { ProgramsPage } from './pages/programs/programs.page';
import { RegisterPage } from './pages/register/register.page';
import { WorkoutsPage } from "./pages/workouts/workouts.page";
import { ExercisesPage } from "./pages/exercises/exercises.page";
import { WorkoutCreateComponent } from './components/workout/workout-create/workout-create.component';
import { ExerciseCreateComponent } from './components/exercise/exercise-create/exercise-create.component';
import { NotFoundError } from 'rxjs';
import { NotFoundPage } from './pages/not-found/not-found.page';

const routes: Routes = [
  {
    path: '',
    component: LoginPage, // Public
  },
  {
    path: 'goals',
    component: GoalsDashboardPage, // Only for logged in users
    canActivate: [AuthGuard]
  },
  {
    path: 'goals/create',
    component: GoalCreatePage, // Only for logged in users
    canActivate: [AuthGuard]
  },
  {
    path: 'goals/:goalId',
    component: GoalDetailPage, // Only logged in users
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfilePage,
    canActivate: [AuthGuard],// Protected by Keycloak auth
  },
  {
    path: 'programs',
    component: ProgramsPage,
    canActivate: [AuthGuard],// Protected by Keycloak auth
  },
  {
    path: 'register',
    component: RegisterPage,
    canActivate: [AuthGuard],// Protected by Keycloak auth
  },
  {
    path: 'contributor/dashboard',
    component: ContributorDashboardPage,
    canActivate: [RoleGuard],// Protected by Keycloak Role: ADMIN
    data: {
      role: "Contributor"
    },
  },
  {
    path: 'contributor/dashboard/workouts/:workoutId',
    component: WorkoutCreateComponent,
    canActivate: [RoleGuard],
    data: {
      role: "Contributor"
    },
  },
  {
    path: 'contributor/dashboard/exercises/:exerciseId',
    component: ExerciseCreateComponent,
    canActivate: [RoleGuard],
    data: {
      role: "Contributor"
    },
  },
  {
    path: 'workouts',
    component: WorkoutsPage,
    canActivate: [AuthGuard],// Protected by Keycloak auth
  },
  {
    path: 'exercises',
    component: ExercisesPage,
    canActivate: [AuthGuard],// Protected by Keycloack auth
  },
  {
    path: '**',
    component: NotFoundPage,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
