import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import keycloak from 'src/keycloak';
import { ProfileManagerService } from '../services/profile-manager.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private readonly profileManager: ProfileManagerService
    ) {

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
      // Authenticated and has a profile
      if (keycloak.authenticated && this.profileManager.GetIfProfileExists()) {
        return true;
      }

      // If the path of the page is register the user wont need an existing profile
      if (keycloak.authenticated && route.routeConfig?.path === "register") {
        return true;
      }

      // Automatically navigate back to Login
      this.router.navigateByUrl("/");
      return false;
  }
  
}
