import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import keycloak from 'src/keycloak';
import { ProfileManagerService } from '../services/profile-manager.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private readonly profileManager: ProfileManagerService) {

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      // The user needs to be authenticated in keycloak and needs an profile
      if (!keycloak.authenticated || !this.profileManager.GetIfProfileExists()) {
        this.router.navigateByUrl("/");
        return false;
      }

      const { role } = route.data;

      if (keycloak.hasRealmRole(role)) {
        return true;
      } 
      else {
        return false;
      }

  }
  
}
