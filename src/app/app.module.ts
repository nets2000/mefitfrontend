import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpAuthInterceptor } from './interceptors/auth-http.interceptor';
import { LoginPage } from './pages/login/login.page';
import { GoalsDashboardPage } from './pages/goals-dashboard/goals-dashboard.page';
import { ProfilePage } from './pages/profile/profile.page';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContributorDashboardPage } from './pages/contributor-dashboard/contributor-dashboard.page';
import { RefreshTokenHttpInterceptor } from './interceptors/refresh-token-http.interceptor';
import { GoalDetailPage } from './pages/goal-detail/goal-detail.page';
import { GoalsHistoryListComponent } from './components/goal/goals-history-list/goals-history-list.component';
import { GoalsHistoryListItemComponent } from './components/goal/goals-history-list-item/goals-history-list-item.component';
import { GoalCreatePage } from './pages/goal-create/goal-create.page';
import { GoalCreateFormComponent } from './components/goal/goal-create-form/goal-create-form.component';
import { GoalsListComponent } from './components/goal/goals-list/goals-list.component';
import { ProgramsPage } from './pages/programs/programs.page';
import { ProgramListComponent } from './components/program/program-list/program-list.component';
import { ProgramDetailsComponent } from './components/program/program-details/program-details.component';
import { WorkoutsPage } from './pages/workouts/workouts.page';
import { WorkoutListComponent } from './components/workout/workout-list/workout-list.component';
import { ExercisesPage } from './pages/exercises/exercises.page';
import { ExerciseListComponent } from './components/exercise/exercise-list/exercise-list.component';
import { ExerciseListItemComponent } from './components/exercise/exercise-list-item/exercise-list-item.component';
import { WorkoutDetailComponent } from './components/workout/workout-detail/workout-detail.component';
import { RegisterPage } from './pages/register/register.page';
import { RegisterformComponent } from './components/register/registerform/registerform.component';
import { RegisterExerciseComponent } from './components/register/register-exercise/register-exercise.component';
import { ExerciseChecklistComponent } from './components/exercise/exercise-checklist/exercise-checklist.component';
import { CdkAccordionModule } from '@angular/cdk/accordion'
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExerciseDetailComponent } from './components/exercise/exercise-detail/exercise-detail.component';
import { GoalsListItemComponent } from './components/goal/goals-list-item/goals-list-item.component';
import { WorkoutCreateComponent } from './components/workout/workout-create/workout-create.component';
import { UserWorkoutsHistoryComponent } from './components/user-workouts-history/user-workouts-history.component';
import { ProgramFormComponent } from './components/program/program-form/program-form.component';
import { ProfileDataComponent } from './components/profile/profile-data/profile-data.component';
import { ProfileFormComponent } from './components/profile/profile-form/profile-form.component';
import { EditProfileComponent } from './components/profile/edit-profile/edit-profile.component';
import { ExerciseCreateComponent } from './components/exercise/exercise-create/exercise-create.component';
import { MusclegroupsChecklistComponent } from './components/musclegroups-checklist/musclegroups-checklist.component';
import { UserExercisesHistoryComponent } from './components/user-exercises-history/user-exercises-history.component';
import { NgChartsModule } from 'ng2-charts';
import { MatTooltipModule } from "@angular/material/tooltip";
import { WorkoutChecklistComponent } from './components/workout/workout-checklist/workout-checklist.component';
import { ErrorComponent } from './components/error/error.component';
import { NotFoundPage } from './pages/not-found/not-found.page';
import { RedirectTimerComponent } from './components/redirect-timer/redirect-timer.component';
import { SuccessComponent } from './components/success/success.component';
import { UserWorkoutsHistoryItemComponent } from './components/user-workouts-history-item/user-workouts-history-item.component';
import { UserExercisesHistoryItemComponent } from './components/user-exercises-history-item/user-exercises-history-item.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    GoalsDashboardPage,
    ProfilePage,
    NavbarComponent,
    ContributorDashboardPage,
    GoalDetailPage,
    GoalsHistoryListComponent,
    GoalsHistoryListItemComponent,
    GoalCreatePage,
    GoalCreateFormComponent,
    GoalsListComponent,
    ProgramsPage,
    ProgramListComponent,
    ProgramDetailsComponent,
    WorkoutsPage,
    WorkoutListComponent,
    WorkoutDetailComponent, 
    ExercisesPage, 
    ExerciseListComponent, 
    ExerciseListItemComponent, 
    ExerciseDetailComponent, 
    RegisterPage, 
    RegisterformComponent, 
    RegisterExerciseComponent, 
    ExerciseChecklistComponent, 
    GoalsListItemComponent, 
    WorkoutCreateComponent, 
    UserWorkoutsHistoryComponent,
    ProgramFormComponent,
    ProfileDataComponent,
    ProfileFormComponent,
    ExerciseCreateComponent,
    MusclegroupsChecklistComponent,
    UserExercisesHistoryComponent,
    EditProfileComponent,
    WorkoutChecklistComponent,
    ErrorComponent,
    NotFoundPage,
    RedirectTimerComponent,
    SuccessComponent,
    UserWorkoutsHistoryItemComponent,
    UserExercisesHistoryItemComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CdkAccordionModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    NgChartsModule
  ],
  providers: [
    // Check Token Before each request
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenHttpInterceptor,
      multi: true,
    },
    // Add HttpAuthInterceptor - Add Bearer Token to request
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
