# [MeFit](https://mefit.azurewebsites.net/)

A fitness app which shows a goal dashboard for the user. <br/>
Here the user can set new goals for the current week or see his goals and goal history.
He can also see all available programs, workouts and exercises.
Programs contain workouts and workouts contain exercises. <br/><br/>
There's also an area for contributors. They can create/edit programs, workouts and exercises. 

![alt text](./src/assets/img/readme/goal-dashboard.jpg)<br>

## Installation
```
git clone https://gitlab.com/nets2000/mefitfrontend.git
```

## Usage
To start the app in a dev server, <br/> 
First install: [Angular CLI](https://www.github.com/angular/angular-cli) to execute:

```
npm install -g @angular/cli
```
Install all npm libraries, by:
```
npm install
```

After installing, you can start the app with:
```
ng serve
```

To use the app without installations, click the following link:
[MeFit Azure](https://mefit.azurewebsites.net/)

To view contributor only pages, use
```
username: Contributor
password: password
```

## Preview

### Login/register page
You'll be redirected to keycloack where you can create an account or login to your existing account.<br><br>
![alt text](./src/assets/img/readme/login-page.jpg)<br>
![alt text](./src/assets/img/readme/login-keycloack.jpg)<br>

### Registration form
When you just registered on keycloack, you'll see a page to create a profile and set your initial fitness rating.

### Goal dashboard
When you're logged in, and you've created a profile, you'll be redirected to the goals dashboard. You can:
* Click on the current goal or goal history to see the details of that goal
* Click on the button to set a new goal.<br>

![alt text](./src/assets/img/readme/goal-dashboard.jpg)<br>

### Available programs, workouts & exercises
Here you can see all the available programs, workouts & exercises provided by the app.<br><br>
![alt text](./src/assets/img/readme/available-programs.jpg)<br>

### Profile page
![alt text](./src/assets/img/readme/profile.jpg)<br>

### Contributor dashboard
Here you can create and edit programs, workouts and exercises. This page is only shown when you're a contributor.<br><br>
![alt text](./src/assets/img/readme/contributor-dashboard.jpg)<br>

## Team Members
Sten de Boer, Nienke Kapers and Fenna Ransijn
